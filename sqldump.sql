-- Table structure for table `account_sessions` 

CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `members` 

CREATE TABLE `members` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `member_type` varchar(20) NOT NULL DEFAULT 'lender',
  `employee` int(1) NOT NULL DEFAULT '0',
  `active` int(20) NOT NULL DEFAULT '1',
  `gender` varchar(10) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthplace` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_mobile` varchar(255) DEFAULT NULL,
  `phone_home` varchar(255) DEFAULT NULL,
  `phone_office` varchar(255) DEFAULT NULL,
  `submitted_by` int(20) DEFAULT NULL,
  `lastmod_by` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `members_account` 

CREATE TABLE `members_account` (
  `mid` int(10) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  UNIQUE KEY `username` (`username`),
  KEY `mid` (`mid`)
);

-- Table structure for table `members_account_access` 

CREATE TABLE `members_account_access` (
  `mid` int(10) NOT NULL,
  `module` varchar(100) NOT NULL,
  `view` int(1) NOT NULL DEFAULT '0',
  `add` int(1) NOT NULL DEFAULT '0',
  `edit` int(1) NOT NULL DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '0',
  KEY `mid` (`mid`)
);

-- Table structure for table `members_address` 

CREATE TABLE `members_address` (
  `member_id` int(20) NOT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `building` varchar(100) DEFAULT NULL,
  `lot_block` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `subdivision` varchar(100) DEFAULT NULL,
  `barangay` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  KEY `id` (`member_id`),
  CONSTRAINT `members_address_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `members_capital` 

CREATE TABLE `members_capital` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) NOT NULL,
  `receipt_number` int(20) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `date_contributed` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `receipt_number` (`receipt_number`)
);

-- Table structure for table `members_loans` 

CREATE TABLE `members_loans` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) NOT NULL,
  `loan_date` date NOT NULL,
  `principal` float(10,2) NOT NULL,
  `payment_start` date NOT NULL,
  `parent` int(20) NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT 'undecided',
  `submitted_by` int(20) DEFAULT NULL,
  `lastmod_by` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `members_loans_interest` 

CREATE TABLE `members_loans_interest` (
  `loan_id` int(20) NOT NULL,
  `interest_rate` float(10,2) NOT NULL,
  `months` int(2) NOT NULL,
  `skip` int(1) NOT NULL DEFAULT '0',
  `type` varchar(100) DEFAULT 'fixed'
);

-- Table structure for table `members_loans_invoices` 

CREATE TABLE `members_loans_invoices` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `loan_id` int(20) NOT NULL,
  `number` int(20) NOT NULL,
  `due_date` date NOT NULL,
  `principal_due` float(10,5) NOT NULL,
  `interest_due` float(10,5) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `members_loans_payment` 

CREATE TABLE `members_loans_payment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `loan_id` int(20) NOT NULL,
  `receipt_number` int(20) NOT NULL,
  `payment_date` date NOT NULL,
  `amount` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `receipt_number` (`receipt_number`)
);

