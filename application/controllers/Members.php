<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends MY_Controller {

	var $module_id = 'members';

	public function __construct() {
		parent::__construct();

		$this->template_data->set('module_id', $this->module_id );
		$this->template_data->set('sub_module_id', 'profile');

		$this->load->model('Members_model');
		$this->load->model('Members_loans_model');
		$this->load->model('Members_loans_interest_model');
		$this->load->model('Members_loans_payment_model');
		$this->load->model('Members_address_model');
		$this->load->model('Members_capital_model');
		$this->load->model('Members_account_model');
		$this->load->model('Members_account_access_model');
		$this->load->model('Members_loans_invoices_model');

		$this->_isAuth($this->module_id, 'view', $this->agent->referrer());

	}

	public function index($type=false, $start=0)
	{

		$this->load->model('Members_model');
		$stats = new $this->Members_model;
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE active = 0) as inactive');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE member_type LIKE "shareholder" AND active = 1) as shareholders');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE member_type LIKE "lender" AND active = 1) as lenders');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE active = 1) as members');
		$this->template_data->set('stats', $stats->get());

		$members = new $this->Members_model;
		$members->setStart($start);
		$members->setOrder('lastname', 'ASC');
		$base_url = site_url('members/index/all/');
		switch( $type ) {
			case 'shareholder':
				$members->setMemberType('shareholder', true);
				$base_url = site_url('members/index/shareholder/');
			break;
			case 'lender':
				$members->setMemberType('lender', true);
				$base_url = site_url('members/index/lender/');
			break;
		}
		if( $this->input->get('q') != '' ) {
			$members->setWhereOr('lastname LIKE "%' . $this->input->get('q') . '%"');
			$members->setWhereOr('firstname LIKE "%' . $this->input->get('q') . '%"');
			$members->setWhereOr('middlename LIKE "%' . $this->input->get('q') . '%"');
		}
		$members->setActive(1,true);
		$this->template_data->set('members', $members->populate());

		$config['base_url'] = $base_url;
		$config['total_rows'] = $members->count_all_results();
		$config['per_page'] = $members->getLimit();

		$this->template_data->set('pagination', bootstrap_pagination($config));

		$this->load->view('members/members', $this->template_data->get_data());
	}

	public function inactive($start=0)
	{

		$this->load->model('Members_model');
		$stats = new $this->Members_model;
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE active = 0) as inactive');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE member_type LIKE "shareholder" AND active = 1) as shareholders');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE member_type LIKE "lender" AND active = 1) as lenders');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE active = 1) as members');
		$this->template_data->set('stats', $stats->get());

		$members = new $this->Members_model;
		$members->setStart($start);
		$members->setOrder('lastname', 'ASC');
		if( $this->input->get('q') != '' ) {
			$members->setWhereOr('lastname LIKE "%' . $this->input->get('q') . '%"');
			$members->setWhereOr('firstname LIKE "%' . $this->input->get('q') . '%"');
			$members->setWhereOr('middlename LIKE "%' . $this->input->get('q') . '%"');
		}
		$members->setActive(0,true);
		$this->template_data->set('members', $members->populate());

		$config['base_url'] = site_url('members/inactive/');
		$config['total_rows'] = $members->count_all_results();
		$config['per_page'] = $members->getLimit();

		$this->template_data->set('pagination', bootstrap_pagination($config));

		$this->load->view('members/members', $this->template_data->get_data());
	}

	public function add()
	{
		
		$this->_isAuth($this->module_id, 'add', $this->agent->referrer());

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middle Name', 'trim|required');
			
			if( $this->form_validation->run() != FALSE) {
				$member = new $this->Members_model;
				$member->setLastname($this->input->post('lastname'));
				$member->setFirstname($this->input->post('firstname'));
				$member->setMiddlename($this->input->post('middlename'));
				$member->setMemberType('lender');
				$member->setEmployee(0);
				$member->setActive(0);
				$member->setBirthdate(date('Y-m-d', strtotime($this->input->post('birthdate'))));
				$member->setBirthplace($this->input->post('birthplace'));
				$member->setGender($this->input->post('gender'));
				$member->setMaritalStatus($this->input->post('marital_status'));
				$member->setSubmittedBy( $this->session->member_id );
				$member->setLastmodBy( $this->session->member_id );
				if( $member->insert() ) {
					redirect('members/profile/' . $member->getId() );
				}
			}
		}
		$this->load->view('members/members_add', $this->template_data->get_data());
	}

	public function delete($id) {

		$this->_isAuth($this->module_id, 'delete', $this->agent->referrer());

		$parish = new $this->Members_model;
		$parish->setId($id, true);
		$parish->delete();
		redirect('members');
	}

	public function profile($id, $view=null)
	{
		$member = new $this->Members_model('m');
		$member->setId($id, true);
		$member->setJoin('members_address a', 'm.id = a.member_id');
		$member->setJoin('members_account c', 'm.id = c.mid');
		$member->setLastmodBy( $this->session->member_id );

		if( count($this->input->post()) > 0 ) {

			$member_data = $member->get();
			if( hasAccess('members', 'edit') || ( ($member_data->submitted_by == $this->session->member_id) && ($member_data->active==0) ) ) {

			switch($this->input->post('action')) {
				case 'personal_info':
					$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
					$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
					$this->form_validation->set_rules('middlename', 'Middle Name', 'trim|required');
					if( $this->form_validation->run() != FALSE) {
						$member->setLastname($this->input->post('lastname'), false, true);
						$member->setFirstname($this->input->post('firstname'), false, true);
						$member->setMiddlename($this->input->post('middlename'), false, true);
						$member->setMiddlename($this->input->post('middlename'), false, true);
						$member->setBirthdate(date('Y-m-d', strtotime($this->input->post('birthdate'))), false, true);
						$member->setBirthplace($this->input->post('birthplace'), false, true);
						$member->setGender($this->input->post('gender'), false, true);
						$member->setMaritalStatus($this->input->post('marital_status'), false, true);
						$member->update();
					}
				break;
				case 'membership':
					$this->form_validation->set_rules('member_type', 'Member Type', 'trim|required');
					if( $this->form_validation->run() != FALSE) {
						$member->setMemberType($this->input->post('member_type'), false, true);
						$member->setActive(($this->input->post('active')?1:0), false, true);
						$member->setEmployee(($this->input->post('employee')?1:0), false, true);
						$member->update();
					}
				break;
				case 'contact_information':
						$member->setPhoneMobile($this->input->post('phone_mobile'), false, true);
						$member->setPhoneHome($this->input->post('phone_home'), false, true);
						$member->setPhoneOffice($this->input->post('phone_office'), false, true);
						$member->setEmail($this->input->post('email'), false, true);
						$member->update();
				break;
				case 'address':
						$address = new $this->Members_address_model;
						$address->setMemberId($id,true);
						$address->setUnit($this->input->post('unit'));
						$address->setBuilding($this->input->post('building'));
						$address->setLotBlock($this->input->post('lot_block'));
						$address->setStreet($this->input->post('street'));
						$address->setSubdivision($this->input->post('subdivision'));
						$address->setBarangay($this->input->post('barangay'));
						$address->setCity($this->input->post('city'));
						$address->setProvince($this->input->post('province'));
						$address->setZip($this->input->post('zip'));
						if($address->nonEmpty()) {
							$address->update();
						} else {
							$address->insert();
						}
				break;
				case 'account':
					$account = new $this->Members_account_model;
					$account->setMid($id, true);

					$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|alpha_numeric');
						if( $account->get() ) {
							$this->form_validation->set_rules('password', 'New Password', 'trim');
							$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|matches[password]');
						} else {
							$this->form_validation->set_rules('password', 'New Password', 'trim|required');
							$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|matches[password]|required');
						}

					if( $this->form_validation->run() != FALSE) {

						$account->setUsername($this->input->post('username'));
						if( ($this->input->post('password') && $this->input->post('password2')) && ($this->input->post('password') == $this->input->post('password2')) ) 
						{
							$account->setPassword(sha1($this->input->post('password')));
						}
						if( $account->nonEmpty() ) {
							$account->setExclude('id');
							$account->update();	
						} else {
							$account->insert();
						}
					}
				break;
				case 'account_access':
					
					foreach(
						array(
						  'summary' => 'Summary',
						  'members'=>'Members',
						  'capitals'=>'Capitals',
						  'loans'=>'Loans',
						  'payments'=>'Payments',
					  ) as $key=>$label) 
					{
					  	$setting = $this->input->post( $key );
					  	$access = new $this->Members_account_access_model;
					  	$access->setMid($id, true);
					  	$access->setModule($key, true);
					  	$access->setView((isset($setting['view'])) ? 1 : 0);
					  	$access->setAdd((isset($setting['add'])) ? 1 : 0);
					  	$access->setEdit((isset($setting['edit'])) ? 1 : 0);
					  	$access->setDelete((isset($setting['delete'])) ? 1 : 0);
					  	if( $access->nonEmpty() ) {
					  		$access->update();
					  	} else {
					  		$access->insert();
					  	}
					}

				break;
			}
			}
		}

		$this->template_data->set('member', $member->get());

		$loans = new $this->Members_loans_model;
		$loans->setMemberId($id,true);
		$loans->setSelect('members_loans.*');
		$loans->setJoin('members_loans_interest i', 'members_loans.id=i.loan_id');
		$loans->setSelect('i.interest_rate, i.months, i.skip');
		$loans->setSelect('(SELECT sum(amount) FROM members_loans_payment p WHERE p.loan_id=members_loans.id) as payments');
		$this->template_data->set('loans', $loans->populate());

		$account_access = new $this->Members_account_access_model;
		$account_access->setLimit(0);
		$account_access->setMid($id,true);
		$this->template_data->set('account_access', $account_access->populate());

		if( $view == 'print') {
			$this->load->view('members/members_profile_print', $this->template_data->get_data());
		} else {
			$this->load->view('members/members_profile', $this->template_data->get_data());
		}
	}

	public function capital($mid, $action=null, $id=false)
	{

		$this->template_data->set('sub_module_id', 'capital');

		$member = new $this->Members_model('m');
		$member->setId($mid, true);
		$member->setSelect('m.*');
		$member->setSelect('(SELECT SUM(c.amount) FROM members_capital c WHERE m.id=c.member_id) as total_capital');
		$this->template_data->set('member', $member->get());

		switch($action) {
			case 'add':

				$this->_isAuth('capitals', 'add', $this->agent->referrer());

				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('date_contributed', 'Date Contributed', 'trim|required');
					$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required|is_unique[members_capital.receipt_number]');
					$this->form_validation->set_rules('amount', 'Amount Contributed', 'trim|required');
					if( $this->form_validation->run() != FALSE) {
						$capital = new $this->Members_capital_model;
						$capital->setMemberId($mid);
						$capital->setReceiptNumber($this->input->post('receipt_number'));
						$capital->setDateContributed(date('Y-m-d', strtotime($this->input->post('date_contributed'))));
						$capital->setAmount( $this->input->post('amount') );
						if( $capital->insert() ) {
							redirect('members/capital/' . $mid . '/update/' . $capital->getId() );
						}
					}
				}

				$this->load->view('members/members_capital_add', $this->template_data->get_data());
			break;

			case 'update':
				$capital = new $this->Members_capital_model;
				$capital->setMemberId($mid, true);
				$capital->setId($id,true);

				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('date_contributed', 'Date Contributed', 'trim|required');
					$this->form_validation->set_rules('amount', 'Amount Contributed', 'trim|required');
					if( $this->form_validation->run() != FALSE) {
						$capital->setDateContributed(date('Y-m-d', strtotime($this->input->post('date_contributed'))));
						$capital->setAmount( $this->input->post('amount') );
						$capital->setReceiptNumber($this->input->post('receipt_number'));
						$capital->update();
					}
				}

				$this->template_data->set('capital', $capital->get());
				$this->load->view('members/members_capital_edit', $this->template_data->get_data());
			break;

			default:
				$capitals = new $this->Members_capital_model;
				$capitals->setMemberId($mid, true);
				$capitals->setOrder('date_contributed', 'DESC');
				$this->template_data->set('capitals', $capitals->populate());
				$this->load->view('members/members_capital', $this->template_data->get_data());
			break;
		}
	}

	private function _nextPaymentDate($today, $skip=15) {
		$todayStamp = strtotime($today);
		$numOfDays = date('t', $todayStamp);
		if( date('d', $todayStamp) > $skip) {
			$base = strtotime('+'.$numOfDays.' days', strtotime(date('Y-m-01', $todayStamp)));
			return date('Y-m-15', $base);
		} else {
			$base = strtotime(date('Y-m-01', $todayStamp));
			return date('Y-m-'.min(date('t', $base), 30), $base);
		}
	}

	private function _setInvoices($loan_id) {
		$loan = new $this->Members_loans_model('ml');
		$loan->setId($loan_id,true);
		$loan->setJoin('members_loans_interest mli', 'ml.id=mli.loan_id');
		$current_loan = $loan->get();

		$installments = ceil((30 / $current_loan->skip) * $current_loan->months);
		$due_date = date('Y-m-d', strtotime($current_loan->payment_start));
		$principal_amount = ($current_loan->principal/$installments);
		$interest = (($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip);
		$principal_diminishing = $current_loan->principal;

		for($i=1;$i<=$installments;$i++) { 

			if( $current_loan->type == 'diminishing' ) {
			  $interest = ((($principal_diminishing * $current_loan->interest_rate) / 100) * (1/ceil((30 / $current_loan->skip))));
			}

			$principal_diminishing -= $principal_amount;

			$invoice = new $this->Members_loans_invoices_model('mli');
			$invoice->setLoanId($loan_id,true);
			$invoice->setNumber($i,true);
			$invoice->setPrincipalDue($principal_amount);
			$invoice->setInterestDue($interest);
			if( $invoice->nonEmpty() == TRUE ) {
				$invoice->setExclude(array('id', 'due_date'));
				$invoice->update();
			} else {
				$invoice->setDueDate($due_date);
				$invoice->insert();
			}

			echo $due_date = $this->_nextPaymentDate($due_date, $current_loan->skip); //date('Y-m-d', (strtotime($due_date . " + " . $current_loan->skip ." days")) );
		}

		$invoices = new $this->Members_loans_invoices_model;
		$invoices->setWhere('number > ', $installments);
		$invoices->delete();
	}

	public function loan($mid, $action=null, $loan_id=false)
	{

		$this->template_data->set('sub_module_id', 'loan');

		$member = new $this->Members_model('m');
		$member->setId($mid, true);
		$member->setJoin('members_address a', 'm.id = a.member_id');
		$member_data = $member->get();
		$this->template_data->set('member', $member_data);

		switch($action) {

			case 'approve':
				$loan = new $this->Members_loans_model;
				$loan->setMemberId($mid,true);
				$loan->setId($loan_id,true);
				$loan->setStatus('approved',false,true);
				$loan->setApprovedBy($this->session->member_id,false,true);
				if($loan->update() ) {
					if( $member_data->active==0 ) {
						$member->setActive(1,false,true);
						$member->setApprovedBy($this->session->member_id,false,true);
						$member->update();
					}
					redirect('members/loan/' . $mid . '/schedule/' .$loan_id );
				}
			break;

			case 'disapprove':
				$loan = new $this->Members_loans_model;
				$loan->setMemberId($mid,true);
				$loan->setId($loan_id,true);
				$loan->setStatus('disapproved',false,true);
				$loan->update();
				redirect('members/loan/' . $mid . '/schedule/' .$loan_id );
			break;

			case 'add':

				$this->_isAuth('loans', 'add', $this->agent->referrer());

				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('loan_date', 'Loan Date', 'trim|required');
					$this->form_validation->set_rules('principal', 'Principal Amount', 'trim|required');
					$this->form_validation->set_rules('payment_start', 'Payment Start', 'trim|required');
					if( $this->form_validation->run() != FALSE) {
						$loan = new $this->Members_loans_model;
						$loan->setMemberId($mid);
						$loan->setLoanDate(date('Y-m-d', strtotime($this->input->post('loan_date'))));
						$loan->setPrincipal($this->input->post('principal'));
						$loan->setPaymentStart(date('Y-m-d', strtotime($this->input->post('payment_start'))));
						$loan->setParent(0);
						$loan->setStatus('undecided');
						$loan->setSubmittedBy($this->session->member_id);
						if( $loan->insert() ) {
							redirect('members/loan/' . $mid . '/schedule/' . $loan->getId() );
						}
					}
				}
				$this->load->view('members/members_loan_add', $this->template_data->get_data());
			break;

			case 'edit':
				
				$this->_isAuth($this->module_id, 'edit', $this->agent->referrer());

				$loan = new $this->Members_loans_model;
				$loan->setMemberId($mid,true);
				$loan->setId($loan_id,true);
				$loan->setLastmodBy($this->session->member_id, false, true);
				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('loan_date', 'Loan Date', 'trim|required');
					$this->form_validation->set_rules('principal', 'Principal Amount', 'trim|required');
					$this->form_validation->set_rules('payment_start', 'Payment Start', 'trim|required');
					if( $this->form_validation->run() != FALSE) {
						$loan->setLoanDate(date('Y-m-d', strtotime($this->input->post('loan_date'))), false, true);
						$loan->setPrincipal($this->input->post('principal'), false, true);
						$loan->setPaymentStart(date('Y-m-d', strtotime($this->input->post('payment_start'))), false, true);
						if( $loan->update() ) {
							$this->_setInvoices($loan_id);
							redirect('members/loan/' . $mid . '/schedule/' . $loan->getId() );
						}
						
					}
				}
				$this->template_data->set('current_loan', $loan->get());
				$this->load->view('members/members_loan_edit', $this->template_data->get_data());
			break;

			case 'payments':
			case 'view':

				$this->_isAuth('payments', 'view', $this->agent->referrer());

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);
				$this->template_data->set('loan_interest', $interest->get());

				$loan = new $this->Members_loans_model;
				$loan->setId($loan_id, true);
				$this->template_data->set('current_loan', $loan->get());

				$payments = new $this->Members_loans_payment_model;
				$payments->setLoanId($loan_id, true);
				$this->template_data->set('payments', $payments->populate());

				$this->load->view('members/members_loan_payments', $this->template_data->get_data());
			break;

			case 'schedule':

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);
				$this->template_data->set('loan_interest', $interest->get());
				
				$loan = new $this->Members_loans_model('l');
				$loan->setId($loan_id, true);
				$loan->setSelect('l.*');
				$loan->setSelect('(SELECT SUM(amount) FROM members_loans_payment p WHERE p.loan_id=l.id) as total_payments');
				$this->template_data->set('current_loan', $loan->get());

				$this->load->view('members/members_loan_schedule', $this->template_data->get_data());
			break;

			case 'details':

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);
				$this->template_data->set('loan_interest', $interest->get());
				
				$loan = new $this->Members_loans_model;
				$loan->setId($loan_id, true);
				$this->template_data->set('current_loan', $loan->get());

				$this->load->view('members/members_loan_details', $this->template_data->get_data());

			break;

			case 'print_schedule':

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);
				$this->template_data->set('loan_interest', $interest->get());
				
				$loan = new $this->Members_loans_model;
				$loan->setId($loan_id, true);
				$this->template_data->set('current_loan', $loan->get());

				$this->load->view('members/members_loan_schedule_print', $this->template_data->get_data());

			break;

			case 'update_interest':

				$this->_isAuth($this->module_id, 'edit', $this->agent->referrer());

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);

				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('interest_rate', 'Interest Rate', 'trim|required');
					$this->form_validation->set_rules('months', 'Number of Months', 'trim|required');
					$this->form_validation->set_rules('skip', 'Payment Skip Days', 'trim|required');
					$this->form_validation->set_rules('type', 'Interest Type', 'trim|required');
					if( $this->form_validation->run() == true) {
						$interest->setInterestRate($this->input->post('interest_rate'));
						$interest->setMonths($this->input->post('months'));
						$interest->setSkip($this->input->post('skip'));
						$interest->setType($this->input->post('type'));
						$interest->setExclude('id');
						if( $interest->nonEmpty() == FALSE) {
							$interest->insert();
						} else {
							$interest->update();
						}
						$this->_setInvoices($loan_id);
					}
					redirect('members/loan/' . $mid . '/schedule/' . $loan_id );
				}
				$this->template_data->set('loan_interest', $interest->get());

				$loan = new $this->Members_loans_model;
				$loan->setId($loan_id, true);
				$this->template_data->set('current_loan', $loan->get());

				$this->load->view('members/members_loan_update_interest', $this->template_data->get_data());
			break;

			case 'payment':

				$this->_isAuth($this->module_id, 'edit', $this->agent->referrer());

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required|is_unique[members_loans_payment.receipt_number]');
					$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
					$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
					if( $this->form_validation->run() == true) {
						$payment = new $this->Members_loans_payment_model;
						$payment->setLoanId($loan_id, true);
						$payment->setReceiptNumber($this->input->post('receipt_number'),true);
						$payment->setPaymentDate(date('Y-m-d', strtotime($this->input->post('payment_date'))));
						$payment->setAmount( str_replace(',', '', $this->input->post('amount')) );
						if( !$payment->nonEmpty() ) {
							$payment->insert();
						}
						//redirect(str_replace('payment', 'schedule', uri_string()));
						redirect("members/loan/{$mid}/payments/{$loan_id}");
					}
				}

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);
				$this->template_data->set('loan_interest', $interest->get());

				$loan = new $this->Members_loans_model;
				$loan->setId($loan_id, true);
				$this->template_data->set('current_loan', $loan->get());

				$this->load->view('members/members_loan_payment', $this->template_data->get_data());
			break;

			case 'payment_update':

				$this->_isAuth($this->module_id, 'edit', $this->agent->referrer());

				if( ! $loan_id ) {
					redirect('members/profile/' . $mid );
				}

				$payment = new $this->Members_loans_payment_model;
				$payment->setId($loan_id, true, false);

				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required');
					$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
					$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
					if( $this->form_validation->run() == true) {
						$payment->setReceiptNumber($this->input->post('receipt_number'));
						$payment->setPaymentDate(date('Y-m-d', strtotime($this->input->post('payment_date'))));
						$payment->setAmount( str_replace(',', '', $this->input->post('amount')) );
						if( $payment->nonEmpty() ) {
							$payment->setExclude(array('id','loan_id'));
							$payment->update();
						}
						redirect("members/loan/{$mid}/payments/{$payment->get()->loan_id}");
					}
				}
				$this->template_data->set('payment', $payment->get());

				$interest = new $this->Members_loans_interest_model;
				$interest->setLoanId($loan_id,true);
				$this->template_data->set('loan_interest', $interest->get());

				$loan = new $this->Members_loans_model;
				$loan->setId($loan_id, true);
				$this->template_data->set('current_loan', $loan->get());

				$this->load->view('members/members_loan_payment_update', $this->template_data->get_data());
			break;

			default:
				
				$loans = new $this->Members_loans_model;
				$loans->setMemberId($mid,true);
				$loans->setSelect('members_loans.*');
				$loans->setJoin('members_loans_interest i', 'members_loans.id=i.loan_id');
				$loans->setSelect('i.interest_rate, i.months, i.skip, i.type');
				$loans->setSelect('(SELECT sum(amount) FROM members_loans_payment p WHERE p.loan_id=members_loans.id) as payments');
				$loans->setOrder('id', 'DESC');
				$this->template_data->set('loans', $loans->populate());
				$this->load->view('members/members_loans', $this->template_data->get_data());

			break;
		}
		
	}
}
