<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function index()
	{
		redirect("account/login");
	}

	protected function _isLoggedIn($uri='account/profile') {
		if( isset($this->session->loggedIn) && $this->session->loggedIn ) {
        	redirect($uri);
        }
	}

	protected function _isNotLoggedIn($uri='account/login') {
		if( ! $this->session->loggedIn || ! isset($this->session->loggedIn) ) {
            $this->session->sess_destroy();
            redirect($uri);
       }
	}

	public function login()
	{

		$this->_isLoggedIn();

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				//echo sha1($this->input->post('password'));
				$this->load->model('Members_account_model');
				$account = new $this->Members_account_model('ma');
				$account->setUsername($this->input->post('username'),true);
				$account->setPassword(sha1($this->input->post('password')),true); 
				$account->setJoin('members m', 'm.id=ma.mid');
				$account->setWhere('m.member_type LIKE "shareholder"');
				$account->setWhere('m.employee = 1');
				$account->setWhere('m.active = 1');
				if( $account->nonEmpty() ) {
					$results = $account->getResults();
					$this->session->set_userdata( 'loggedIn', true );
					$this->session->set_userdata( 'member_id', $results->mid );
					$this->session->set_userdata( 'username', $results->username );
					$this->session->set_userdata( 'firstname', $results->firstname );
					$this->session->set_userdata( 'lastname', $results->lastname );
					$this->session->set_userdata( 'member_type', $results->member_type );

					if( $results->member_type == 'shareholder' ) {
						$this->load->model('Members_account_access_model');
		                $access = new $this->Members_account_access_model;
		                $access->setMid($this->session->member_id,true);
		                $access->setLimit(0);
		                $session_auth = array();
		                foreach($access->populate() as $auth) {
		                	$session_auth[$auth->module] = (object) array(
		                		'view' => $auth->view,
		                		'add' => $auth->add,
		                		'edit' => $auth->edit,
		                		'delete' => $auth->delete,
		                		);
		                }
		                $this->session->set_userdata('session_auth',  (object) $session_auth );
	            	}
	            	
					redirect('welcome');
				}
			}
		}
		$this->load->view('account/login', $this->template_data->get_data());
	}

	public function change_password()
	{

		$this->_isNotLoggedIn();

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
			$this->form_validation->set_rules('repeat_password', 'Repeat Password', 'trim|required|matches[new_password]');
			if( $this->form_validation->run() != FALSE) {
				$this->load->model('Members_account_model');
				$account = new $this->Members_account_model('ma');
				$account->setMid($this->session->member_id,true);
				$account->setPassword(sha1($this->input->post('new_password'))); 
				$account->setWhere('password LIKE', sha1($this->input->post('current_password')));
				if( $account->nonEmpty() ) {
					$account->setExclude( array('mid', 'username') );
					$account->update();
					redirect('account/change_password');
				}
			}
		}
		$this->load->view('account/change_password', $this->template_data->get_data());
	}

	public function profile() 
	{

		$this->_isNotLoggedIn();

		$this->template_data->set('sub_module_id', 'profile');

		$this->load->model('Members_model');
		$member = new $this->Members_model('m');
		$member->setId($this->session->member_id, true);
		$member->setJoin('members_address a', 'm.id = a.member_id');

		$this->template_data->set('member', $member->get());

		$this->load->view('account/account_profile', $this->template_data->get_data());
	}

	public function loans() 
	{

		$this->_isNotLoggedIn();

		$this->template_data->set('sub_module_id', 'loan');

		$this->load->model('Members_model');
		$member = new $this->Members_model('m');
		$member->setId($this->session->member_id, true);
		$member->setJoin('members_address a', 'm.id = a.member_id');
		$member->setJoin('members_account c', 'm.id = c.mid');
		$this->template_data->set('member', $member->get());

		$this->load->model('Members_loans_model');
		$loans = new $this->Members_loans_model;
		$loans->setMemberId($this->session->member_id,true);
		$loans->setSelect('members_loans.*');
		$loans->setJoin('members_loans_interest i', 'members_loans.id=i.loan_id');
		$loans->setSelect('i.interest_rate, i.months, i.skip, i.type');
		$loans->setSelect('(SELECT sum(amount) FROM members_loans_payment p WHERE p.loan_id=members_loans.id) as payments');
		$this->template_data->set('loans', $loans->populate());

		$this->load->view('account/account_loans', $this->template_data->get_data());
	}

	public function loan($loan_id, $action='payments') 
	{

		$this->_isNotLoggedIn();

		$this->template_data->set('sub_module_id', 'loan');

		$this->load->model('Members_loans_model');
		$loan = new $this->Members_loans_model('l');
		$loan->setId($loan_id, true);
		$loan->setMemberId($this->session->member_id, true);

		if( $loan->nonEmpty() == FALSE ) {
			redirect($this->agent->referrer());
		}

		$loan->setSelect('l.*');
		$loan->setSelect('(SELECT SUM(amount) FROM members_loans_payment p WHERE p.loan_id=l.id) as total_payments');
		$this->template_data->set('current_loan', $loan->get());

		$this->load->model('Members_model');
		$member = new $this->Members_model('m');
		$member->setId($this->session->member_id, true);
		$member->setJoin('members_address a', 'm.id = a.member_id');
		$member->setJoin('members_account c', 'm.id = c.mid');
		$this->template_data->set('member', $member->get());

		$this->load->model('Members_loans_interest_model');
		$interest = new $this->Members_loans_interest_model;
		$interest->setLoanId($loan_id,true);
		$this->template_data->set('loan_interest', $interest->get());

		$this->load->model('Members_loans_payment_model');
		$payments = new $this->Members_loans_payment_model;
		$payments->setLoanId($loan_id, true);
		$this->template_data->set('payments', $payments->populate());

		switch($action) {
			case 'details':
				$this->load->view('account/account_loan_details', $this->template_data->get_data());
			break;
			default:
			case 'schedule':
				$this->load->view('account/account_loan_schedule', $this->template_data->get_data());
			break;
			default:
			case 'payments':
				$this->load->view('account/account_loan_payments', $this->template_data->get_data());
			break;
		}
	}

	public function capital() 
	{

		$this->_isNotLoggedIn();

		$this->template_data->set('sub_module_id', 'capital');

		$this->load->model('Members_model');
		$member = new $this->Members_model('m');
		$member->setId($this->session->member_id, true);
		$member->setJoin('members_address a', 'm.id = a.member_id');
		$member->setJoin('members_account c', 'm.id = c.mid');
		$this->template_data->set('member', $member->get());

		$this->load->view('account/account_capitals', $this->template_data->get_data());
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect("account/login");
	}
}
