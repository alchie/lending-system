<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('Members_model');
		$this->load->model('Members_loans_model');
		$this->load->model('Members_loans_payment_model');
	}

	protected function _summary()
	{

		$stats = new $this->Members_model;
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE member_type LIKE "shareholder") as shareholders');
		$stats->setSelect('(SELECT COUNT(*) FROM members WHERE member_type LIKE "lender") as lenders');
		$stats->setSelect('(SELECT SUM(amount) FROM members_capital) as contributed_capital');
		$stats->setSelect('(SELECT SUM(principal) FROM members_loans) as borrowed_funds');
		$stats->setSelect('(SELECT SUM(amount) FROM members_loans_payment) as total_payments');
		$stats->setSelect('(SELECT  
SUM((SELECT p.amount * ((SELECT IF( (i.type LIKE "fixed"), ((l.principal * (((i.interest_rate / CEIL(30 / i.skip)) * (CEIL((30 / i.skip) * i.months))) / 100)))  , ((((l.principal * (i.interest_rate / 100) ) / 2 ) * ((CEIL((30 / i.skip) * i.months)) + 1) * (1/CEIL(30 / i.skip) ))) ) ) / ((SELECT IF( (i.type LIKE "fixed"), ((l.principal * (((i.interest_rate / CEIL(30 / i.skip)) * (CEIL((30 / i.skip) * i.months))) / 100)))  , ((((l.principal * (i.interest_rate / 100) ) / 2 ) * ((CEIL((30 / i.skip) * i.months)) + 1) * (1/CEIL(30 / i.skip) ))) ) ) + l.principal) ) ) ) 
FROM members_loans_payment p 
JOIN members_loans l ON p.loan_id = l.id 
JOIN members_loans_interest i ON p.loan_id = i.loan_id ) as interest_income');

		$this->template_data->set('stats', $stats->get());

		
		$loans = new $this->Members_loans_model('l');
		$loans->setSelect('l.*');
		$loans->setJoin('members_loans_interest i', 'l.id=i.loan_id');
		$loans->setJoin('members m', 'l.member_id=m.id');
		$loans->setSelect('m.firstname, m.lastname, m.middlename');
		$loans->setSelect('i.interest_rate, i.months, i.skip, i.type');
		$loans->setSelect('(SELECT IF((sum(amount)),sum(amount),0) FROM members_loans_payment p WHERE p.loan_id=l.id) as payments');
				
		// fixed
		$loans->setSelect('(SELECT (l.principal * (((i.interest_rate / CEIL(30 / i.skip)) * (CEIL((30 / i.skip) * i.months))) / 100))) as total_interest');
		
		// diminishing
		$loans->setSelect('(SELECT(((l.principal * (i.interest_rate / 100) ) / 2 ) * ((CEIL((30 / i.skip) * i.months)) + 1) * (1/CEIL(30 / i.skip) ))) total_interest2');

		$loans->setSelect('(SELECT IF( (i.type LIKE "fixed"), ((l.principal + total_interest) - payments), ((l.principal + total_interest2) - payments) ) )  as balance');

		$loans->setOrder('l.principal', 'DESC');
		$loans->setHaving('balance > 0');
		$loans->setLimit(10);
		$this->template_data->set('loans', $loans->populate());
		
	}

	protected function _mySummary()
	{

		$stats = new $this->Members_model('m');
		$stats->setId($this->session->member_id,true);
		$stats->setSelect('(SELECT SUM(amount) FROM members_capital c WHERE c.member_id=m.id) as contributed_capital');
		$stats->setSelect('(SELECT SUM(principal) FROM members_loans l WHERE l.member_id=m.id) as borrowed_funds');
		$stats->setSelect('(SELECT SUM(p.amount) FROM members_loans_payment p JOIN members_loans l ON p.loan_id=l.id WHERE l.member_id=m.id) as total_payments');

		$this->template_data->set('stats', $stats->get());

		$payments = new $this->Members_loans_payment_model('lp');
		$payments->setJoin('members_loans l', 'l.id=lp.loan_id');
		$payments->setWhere('l.member_id =', $this->session->member_id);
		$this->template_data->set('payments', $payments->populate());
	}

	public function index() {
		$this->_mySummary();
		$this->load->view('welcome/my_summary', $this->template_data->get_data());
	}
}
