<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loans extends MY_Controller {
	
	var $module_id = 'loans';
	
	public function __construct() {
		parent::__construct();
		$this->_isAuth($this->module_id, 'view', $this->agent->referrer());
	}

	public function index($start=0)
	{

		$this->load->model('Members_loans_model');
		$loans = new $this->Members_loans_model('l');
		$loans->setSelect('l.*');
		$loans->setJoin('members_loans_interest i', 'l.id=i.loan_id');
		$loans->setJoin('members m', 'l.member_id=m.id');
		$loans->setSelect('m.firstname, m.lastname, m.middlename');
		$loans->setSelect('i.interest_rate, i.months, i.skip, i.type');
		$loans->setSelect('(SELECT IF((sum(amount)),sum(amount),0) FROM members_loans_payment p WHERE p.loan_id=l.id) as payments');
				
		// fixed
		$loans->setSelect('(SELECT (l.principal * (((i.interest_rate / CEIL(30 / i.skip)) * (CEIL((30 / i.skip) * i.months))) / 100))) as total_interest');
		
		// diminishing
		$loans->setSelect('(SELECT(((l.principal * (i.interest_rate / 100) ) / 2 ) * ((CEIL((30 / i.skip) * i.months)) + 1) * (1/CEIL(30 / i.skip) ))) total_interest2');

		$loans->setSelect('(SELECT IF( (i.type LIKE "fixed"), ((l.principal + total_interest) - payments), ((l.principal + total_interest2) - payments) ) )  as balance');
		$loans->setStatus('approved',true);
		$loans->setOrder('l.principal', 'DESC');
		//$loans->setHaving('balance > 0');
		$loans->setLimit(10);
		$loans->setStart($start);
		$this->template_data->set('loans', $loans->populate());

		$config['base_url'] = site_url('loans/index/');
		$config['total_rows'] = $loans->count_all_results();
		$config['per_page'] = $loans->getLimit();

		$this->template_data->set('pagination', bootstrap_pagination($config));

		$this->load->view('loans', $this->template_data->get_data());
	}
}
