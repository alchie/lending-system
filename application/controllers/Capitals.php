<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capitals extends MY_Controller {
	
	var $module_id = 'capitals';

	public function __construct() {
		parent::__construct();
		$this->_isAuth($this->module_id, 'view', $this->agent->referrer());
	}

	public function index($start=0)
	{

		$this->load->model('Members_capital_model');
		$capitals = new $this->Members_capital_model('c');
		$capitals->setJoin('members m', 'c.member_id=m.id');
		$capitals->setSelect('c.*');
		$capitals->setSelect('m.firstname, m.lastname, m.middlename');
		$capitals->setOrder('date_contributed', 'DESC');
		$capitals->setStart($start);
		$capitals->setLimit(10);
		$this->template_data->set('capitals', $capitals->populate());

		$config['base_url'] = site_url('capitals/index/');
		$config['total_rows'] = $capitals->count_all_results();
		$config['per_page'] = $capitals->getLimit();

		$this->template_data->set('pagination', bootstrap_pagination($config));

		$this->load->view('capitals', $this->template_data->get_data());
	}
}
