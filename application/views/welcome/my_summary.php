<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Total Contributed Capital</h3>
        </div>
        <div class="panel-body text-center stat-font">
          <?php echo ($stats) ? number_format($stats->contributed_capital,2) : 0.00; ?>
        </div>
      </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Total Borrowed Funds</h3>
        </div>
        <div class="panel-body text-center stat-font">
        <?php echo ($stats) ? number_format($stats->borrowed_funds,2) : 0.00; ?>
          
        </div>
      </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Total Payments</h3>
        </div>
        <div class="panel-body text-center stat-font"><?php echo ($stats) ? number_format($stats->total_payments,2) : 0.00; ?></div>
      </div>
      </div>
    </div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">My Payments</h3>
      </div>
      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>Date Paid</th>
              <th>Receipt Number</th>
              <th>Amount Paid</th>
              <th width="4%">Action</th>
            </tr>
          </thead>
          <tbody>
<?php foreach($payments as $payment) { 
            


            ?>
            <tr>
              <td><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?></td>
              <td><?php echo $payment->receipt_number; ?></td>
              <td><?php echo number_format($payment->amount,2); ?></td>
              <td><a href="<?php echo site_url( "account/loan/" . $payment->loan_id  ); ?>" class="btn btn-warning btn-xs">View Loan</a></td>
            </tr>
<?php } ?>
          </tbody>
        </table>
        
      </div>

<div class="list-group visible-xs">
        <?php foreach($payments as $loan) { 

if( $payment->type == 'diminishing' ) {
  $total_interest = $payment->total_interest2;
} else {
  $total_interest = $payment->total_interest;
}

          ?>
          <a href="<?php echo site_url( "members/loan/" . $payment->member_id . "/view/" . $payment->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format((($payment->principal+$total_interest)-$payment->payments),2); ?></span>
              <div class="font70p"><?php echo $payment->lastname; ?>, <?php echo $payment->firstname; ?></div>
          </a>
        <?php } ?>
        </div>

    </div>
  </div>
</div>
</div>

<?php $this->load->view('footer'); ?>