<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Available Balance</h3>
        </div>
        <div class="panel-body text-center stat-font">
<?php echo ($stats) ? number_format((($stats->contributed_capital - $stats->borrowed_funds) + $stats->total_payments),2) : 0.00; ?>
        </div>
      </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Total Contributed Capital</h3>
        </div>
        <div class="panel-body text-center stat-font">
          <?php echo ($stats) ? number_format($stats->contributed_capital,2) : 0.00; ?>
        </div>
      </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Total Borrowed Funds</h3>
        </div>
        <div class="panel-body text-center stat-font">
        <?php echo ($stats) ? number_format($stats->borrowed_funds,2) : 0.00; ?>
          
        </div>
      </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Total Income</h3>
        </div>
        <div class="panel-body text-center stat-font"><?php echo ($stats) ? number_format($stats->interest_income,2) : 0.00; ?></div>
      </div>
      </div>
    </div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Outstanding Loan Balances</h3>
      </div>
      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>Lender</th>
              <th>Principal</th>
              <th>Interest</th>
              <th>Interest Type</th>
              <th>Paid</th>
              <th>Balance</th>
              <th width="4%">Action</th>
            </tr>
          </thead>
          <tbody>
<?php foreach($loans as $loan) { 
            
if( $loan->type == 'diminishing' ) {
  $total_interest = $loan->total_interest2;
} else {
  $total_interest = $loan->total_interest;
}

            ?>
            <tr>
              <td><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo ($loan->middlename!='') ? substr($loan->middlename,0,1)."." : ''; ?></td>
              <td><?php echo number_format($loan->principal,2); ?></td>
              <td><?php echo number_format($total_interest,2); ?></td>
              <td><?php echo ucwords($loan->type); ?></td>
              <td><?php echo number_format($loan->payments,2); ?></td>
              <td><?php echo number_format((($loan->principal+$total_interest)-$loan->payments),2); ?></td>
              <td><a href="<?php echo site_url( "members/loan/" . $loan->member_id . "/view/" . $loan->id ); ?>" class="btn btn-warning btn-xs">View Loan</a></td>
            </tr>
<?php } ?>
          </tbody>
        </table>
        
      </div>

<div class="list-group visible-xs">
        <?php foreach($loans as $loan) { 

if( $loan->type == 'diminishing' ) {
  $total_interest = $loan->total_interest2;
} else {
  $total_interest = $loan->total_interest;
}

          ?>
          <a href="<?php echo site_url( "members/loan/" . $loan->member_id . "/view/" . $loan->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format((($loan->principal+$total_interest)-$loan->payments),2); ?></span>
              <div class="font70p"><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?></div>
          </a>
        <?php } ?>
        </div>

    </div>
  </div>
</div>
</div>

<?php $this->load->view('footer'); ?>