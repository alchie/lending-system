<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
  <div class="col-md-12">

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo ($member->middlename!='') ? substr($member->middlename,0,1)."." : ''; ?></span>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="<?php echo ($sub_module_id=='profile') ? 'active' : ''; ?>">
        <a href="<?php echo site_url("members/profile/" . $member->id); ?>">Profile</a></li>
<?php if( hasAccess('capitals', 'view') ) { ?>
        <?php if( $member->member_type == 'shareholder' ) { ?>
        <li class="<?php echo ($sub_module_id=='capital') ? 'active' : ''; ?>">
        <a href="<?php echo site_url("members/capital/" . $member->id); ?>">Capital</a></li>
        <?php }  ?>
<?php }  ?>
<?php if( hasAccess('loans', 'view') ) { ?>
        <li class="<?php echo ($sub_module_id=='loan') ? 'active' : ''; ?>">
        <a href="<?php echo site_url("members/loan/" . $member->id); ?>">Loans</a></li>
<?php }  ?>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

  </div>
</div>