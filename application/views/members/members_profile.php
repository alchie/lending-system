<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );

$aa = array();
foreach($account_access as $access) {
  $aa[$access->module]['view'] = $access->view;
  $aa[$access->module]['add'] = $access->add;
  $aa[$access->module]['edit'] = $access->edit;
  $aa[$access->module]['delete'] = $access->delete;
}

?>
<?php $this->load->view('header'); ?>
    <div class="container">

<?php $this->load->view('members/members_navbar'); ?>

<div class="row">
  <div class="col-md-12">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

    <div class="panel panel-default">
      <div class="panel-heading">
      <!--<a href="<?php echo site_url("members/profile/{$member->id}/print"); ?>" class="btn btn-primary btn-xs pull-right" style="margin-left:10px;" target="_print">Print Profile</a>-->
<?php if( hasAccess('members', 'edit') || ( hasAccess('members', 'add') && ($member->submitted_by == $this->session->member_id) && ($member->active==0) ) ) { ?>
      <button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updatePersonalInfo">Update</button>
<?php } ?>
        <h3 class="panel-title">Personal Information</h3>
      </div>

      <div class="panel-body">
            <input type="hidden" name="action" value="personal_info">

<div class="row">
  <div class="col-md-4">
      
        <div class="form-group">
            <label class="control-label">Last Name</label>
            <div class="form-control"><?php echo $member->lastname; ?></div>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">First Name</label>
            <div class="form-control"><?php echo $member->firstname; ?></div>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Middle Name</label>
            <div class="form-control"><?php echo $member->middlename; ?></div>
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Birthday</label>
            <div class="form-control"><?php echo date('F d, Y', strtotime($member->birthdate)); ?></div>
        </div>
  </div>
  <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Birth Place</label>
            <div class="form-control"><?php echo ucwords($member->birthplace); ?></div>
        </div>
  </div>
    <div class="col-md-3">
         <div class="form-group">
            <label class="control-label">Gender</label>
            <div class="form-control"><?php echo ucwords($member->gender); ?></div>
        </div>
  </div>
  <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Marital Status</label>
            <div class="form-control"><?php echo $marital_statuses[$member->marital_status]; ?></div>
        </div>
  </div>
</div>
      </div>
      
    </div>

  </div>
</div>  

<div class="row">
<div class="col-md-12">
    <div class="panel panel-default">
          <div class="panel-heading">
<?php if( hasAccess('members', 'edit') || ( hasAccess('members', 'add') && ($member->submitted_by == $this->session->member_id) && ($member->active==0) ) ) { ?>
<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateAddress">Update</button>
<?php } ?>
            <h3 class="panel-title">Address</h3>
          </div>
          <div class="panel-body">
<div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Unit / Room No. &amp; Floor</label>
              <div class="form-control"><?php echo ucwords($member->unit); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Building Name</label>
              <div class="form-control"><?php echo ucwords($member->building); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Lot No. &amp; Block No. &amp; Phase No.</label>
              <div class="form-control"><?php echo ucwords($member->lot_block); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Street</label>
              <div class="form-control"><?php echo ucwords($member->street); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Subdivision</label>
              <div class="form-control"><?php echo ucwords($member->subdivision); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Barangay</label>
              <div class="form-control"><?php echo ucwords($member->barangay); ?></div>
            </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Municipality/City</label>
              <div class="form-control"><?php echo ucwords($member->city); ?></div>
            </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Province</label>
              <div class="form-control"><?php echo ucwords($member->province); ?></div>
            </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Zip Code</label>
              <div class="form-control"><?php echo ucwords($member->zip); ?></div>
            </div>
          </div>
</div>
          </div>
    </div>
  </div>

</div>
<div class="row">

<div class="col-md-4">
    <div class="panel panel-default">
          <div class="panel-heading">
<?php if( hasAccess('members', 'edit') || ( hasAccess('members', 'add') && ($member->submitted_by == $this->session->member_id) && ($member->active==0) ) ) { ?>
<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateContactInformation">Update</button>
<?php } ?>
            <h3 class="panel-title">Contact Information</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label">Mobile Phone Number</label>
              <div class="form-control"><?php echo $member->phone_mobile; ?></div>
            </div>
            <div class="form-group">
              <label class="control-label">Home Phone Number</label>
              <div class="form-control"><?php echo $member->phone_home; ?></div>
            </div>
            <div class="form-group">
              <label class="control-label">Office Phone Number</label>
              <div class="form-control"><?php echo $member->phone_office; ?></div>
            </div>
            <div class="form-group">
              <label class="control-label">Email Address</label>
              <div class="form-control"><?php echo $member->email; ?></div>
            </div>
          </div>
    </div>
  </div>

<?php if( hasAccess('members', 'edit') ) { ?>

    <div class="col-md-4">
    <div class="panel panel-<?php echo ($member->active==1) ? 'default' : 'danger'; ?>">
          <div class="panel-heading">
<?php if( hasAccess('members', 'edit') || ( hasAccess('members', 'add') && ($member->submitted_by == $this->session->member_id) && ($member->active==0) ) ) { ?>
<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateMembership">Update</button>
<?php } ?>
            <h3 class="panel-title">Membership</h3>
          </div>
          <div class="panel-body">
        <input type="hidden" name="action" value="membership">    
        <div class="form-group">
            <label class="control-label">Membership Type</label>
            <div class="form-control"><?php echo ucwords($member->member_type); ?></div>
        </div>
        <div class="form-group">

            <div class="form-control col-md-6"><i class="glyphicon glyphicon-<?php echo ($member->active==1) ? 'ok green' : 'remove red'; ?>"></i> Active
            <i class="glyphicon glyphicon-<?php echo ($member->employee==1) ? 'ok green' : 'remove red'; ?>"></i> Employee
            </div>
        </div>

          </div>
    </div>

<?php if( $member->active == 1 ) { ?>

        <div class="panel panel-default">
          <div class="panel-heading">
<?php if( hasAccess('members', 'edit') || ( hasAccess('members', 'add') && ($member->submitted_by == $this->session->member_id) && ($member->active==0) ) ) { ?>
<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateAccount">Update</button>
<?php } ?>
            <h3 class="panel-title">Account</h3>
          </div>
          <div class="panel-body">
        <div class="form-group">
            <label class="control-label">Username</label>
            <div class="form-control"><?php echo $member->username; ?></div>
        </div>
          </div>
    </div>

<?php } ?>

  </div>


    <div class="col-md-4">


<?php if( ($member->member_type == 'shareholder') && ( $member->active == 1 ) ) { ?>
  
<?php if( hasAccess('members', 'edit') ) { ?>

    <div class="panel panel-default">
          <div class="panel-heading">
<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateAccountAccess">Update</button>

            <h3 class="panel-title">Account Access</h3>
          </div>
          <div class="panel-body"> 
              <table class="table table-default">
                <thead>
                  <tr>
                    <th>Module</th>
                    <th class="text-center"><span class="hidden-xs">View</span><span class="visible-xs">V</span></th>
                    <th class="text-center"><span class="hidden-xs">Add</span><span class="visible-xs">A</span></th>
                    <th class="text-center"><span class="hidden-xs">Edit</span><span class="visible-xs">E</span></th>
                    <th class="text-center"><span class="hidden-xs">Delete</span><span class="visible-xs">D</span></th>
                  </tr>
                </thead>
                <tbody>
<?php 
foreach(array(
  'summary' => 'Summary',
  'members'=>'Members',
  'capitals'=>'Capitals',
  'loans'=>'Loans',
  'payments'=>'Payments',
  ) as $id=>$label) { ?>
                  <tr>
                    <td><?php echo $label; ?></td>
                    <td class="text-center"><i class="glyphicon glyphicon-<?php echo (isset($aa[$id]) && ($aa[$id]['view']==1)) ? 'ok green' : 'remove red'; ?>"></i></td>
                    <td class="text-center"><i class="glyphicon glyphicon-<?php echo (isset($aa[$id]) && ($aa[$id]['add']==1)) ? 'ok green' : 'remove red'; ?>"></i></td>
                    <td class="text-center"><i class="glyphicon glyphicon-<?php echo (isset($aa[$id]) && ($aa[$id]['edit']==1)) ? 'ok green' : 'remove red'; ?>"></i></td>
                    <td class="text-center"><i class="glyphicon glyphicon-<?php echo (isset($aa[$id]) && ($aa[$id]['delete']==1)) ? 'ok green' : 'remove red'; ?>"></i></td>
                  </tr>
<?php } ?>
                </tbody>
              </table>
          </div>
    </div>
<?php } ?>

  </div>
<?php } ?>
<?php } ?>
</div>

<?php if( hasAccess('members', 'edit') || ( hasAccess('members', 'add') && ($member->submitted_by == $this->session->member_id) && ($member->active==0) ) ) { ?>

<!-- #updatePersonalInfo Modal -->
<div class="modal fade" id="updatePersonalInfo" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Personal Information</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="personal_info">
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Last Name</label>
            <input type="text" class="form-control" name="lastname" value="<?php echo $member->lastname; ?>" required="required">
        </div>
        <div class="form-group">
            <label class="control-label">First Name</label>
            <input type="text" class="form-control" name="firstname" value="<?php echo $member->firstname; ?>" required="required">
        </div>
        <div class="form-group">
            <label class="control-label">Middle Name</label>
            <input type="text" class="form-control" name="middlename" value="<?php echo $member->middlename; ?>" required="required">
        </div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Birthday</label>
           <input type="text" class="form-control datepicker" name="birthdate" value="<?php echo date('m/d/Y', strtotime($member->birthdate)); ?>">
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
             <label class="control-label">Birth Place</label>
           <input type="text" class="form-control" name="birthplace" value="<?php echo $member->birthplace; ?>">
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Gender</label>
            <select class="form-control" name="gender">
              <option value="male" <?php echo ($member->gender=='male') ? "selected" : ""; ?>>Male</option>
              <option value="female" <?php echo ($member->gender=='female') ? "selected" : ""; ?>>Female</option>
            </select>
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Marital Status</label>
            <select class="form-control" name="marital_status">
            <?php foreach($marital_statuses as $key=>$value) { ?>
              <option value="<?php echo $key; ?>" <?php echo ($member->marital_status==$key) ? "selected" : ""; ?>><?php echo $value; ?></option>
              <?php } ?>
            </select>
        </div>
  </div>
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<!-- #updateContactInformation Modal -->
<div class="modal fade" id="updateContactInformation" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Contact Information</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="contact_information">
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Mobile Phone Number</label>
            <input type="text" class="form-control" name="phone_mobile" value="<?php echo $member->phone_mobile; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Home Phone Number</label>
            <input type="text" class="form-control" name="phone_home" value="<?php echo $member->phone_home; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Office Phone Number</label>
            <input type="text" class="form-control" name="phone_office" value="<?php echo $member->phone_office; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Email Address</label>
            <input type="text" class="form-control" name="email" value="<?php echo $member->email; ?>">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<!-- #updateAddress Modal -->
<div class="modal fade" id="updateAddress" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Address</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="address">
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Unit/Room No. &amp; Floor</label>
            <input type="text" class="form-control" name="unit" value="<?php echo $member->unit; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Building Name</label>
            <input type="text" class="form-control" name="building" value="<?php echo $member->building; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Lot No. &amp; Block No. &amp; Phase No.</label>
            <input type="text" class="form-control" name="lot_block" value="<?php echo $member->lot_block; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Street</label>
            <input type="text" class="form-control" name="street" value="<?php echo $member->street; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Subdivision</label>
            <input type="text" class="form-control" name="subdivision" value="<?php echo $member->subdivision; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Barangay</label>
            <input type="text" class="form-control" name="barangay" value="<?php echo $member->barangay; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Municipality / City</label>
            <input type="text" class="form-control" name="city" value="<?php echo $member->city; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Province</label>
            <input type="text" class="form-control" name="province" value="<?php echo $member->province; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">Zip Code</label>
            <input type="text" class="form-control" name="zip" value="<?php echo $member->zip; ?>">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php } ?>

<?php if( hasAccess('members', 'edit') ) { ?>
<!-- #updateMembership Modal -->
<div class="modal fade" id="updateMembership" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Membership</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="membership">
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Member Type</label>
            <select class="form-control" name="member_type" required="required">
              <option value="lender" <?php echo ($member->member_type=='lender') ? "selected" : ""; ?>>Lender</option>
              <option value="shareholder" <?php echo ($member->member_type=='shareholder') ? "selected" : ""; ?>>Shareholder</option>
            </select>
        </div>
        <div class="form-group">
          <label><input type="checkbox" name="active" value="1" <?php echo ($member->active==1) ? "checked" : ""; ?>> Active</label>
        </div>
        <div class="form-group">
          <label><input type="checkbox" name="employee" value="1" <?php echo ($member->employee==1) ? "checked" : ""; ?>> Employee</label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php if( $member->active == 1 ) { ?>

<!-- #updateAccount Modal -->
<div class="modal fade" id="updateAccount" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Account</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="account">
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Username</label>
            <input type="text" class="form-control" name="username" value="<?php echo $member->username; ?>">
        </div>
        <div class="form-group">
            <label class="control-label">New Password</label>
            <input type="password" class="form-control" name="password" value="">
        </div>
        <div class="form-group">
            <label class="control-label">Repeat Password</label>
            <input type="password" class="form-control" name="password2" value="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<!-- #updateAccountAccess Modal -->
<div class="modal fade" id="updateAccountAccess" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Account Access</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="account_access">
      <div class="modal-body">
          <div class="form-group"> 
              <table class="table table-default">
                <thead>
                  <tr>
                    <th>Module</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Add</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                  </tr>
                </thead>
                <tbody>
<?php 

foreach(array(
  'summary' => 'Summary',
  'members'=>'Members',
  'capitals'=>'Capitals',
  'loans'=>'Loans',
  'payments'=>'Payments',
  ) as $id=>$label) { ?>
                  <tr>
                    <td><?php echo $label; ?></td>
                    <td class="text-center"><input type="checkbox" name="<?php echo $id; ?>[view]" value="1" <?php echo (isset($aa[$id]) && ($aa[$id]['view']==1)) ? 'checked' : ''; ?>></td>
                    <td class="text-center"><input type="checkbox" name="<?php echo $id; ?>[add]" value="1" <?php echo (isset($aa[$id]) && ($aa[$id]['add']==1)) ? 'checked' : ''; ?>></td>
                    <td class="text-center"><input type="checkbox" name="<?php echo $id; ?>[edit]" value="1" <?php echo (isset($aa[$id]) && ($aa[$id]['edit']==1)) ? 'checked' : ''; ?>></td>
                    <td class="text-center"><input type="checkbox" name="<?php echo $id; ?>[delete]" value="1" <?php echo (isset($aa[$id]) && ($aa[$id]['delete']==1)) ? 'checked' : ''; ?>></td>
                  </tr>
<?php } ?>
                </tbody>
              </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php } ?>

<?php } ?>

    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>