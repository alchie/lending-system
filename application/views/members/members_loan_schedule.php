<?php defined('BASEPATH') OR exit('No direct script access allowed'); 


function nextPaymentDate($today) {
  $todayStamp = strtotime($today);
  $numOfDays = date('t', $todayStamp);
  if( date('d', $todayStamp) > 15) {
    $base = strtotime('+'.$numOfDays.' days', strtotime(date('Y-m-01', $todayStamp)));
    return date('m/15/Y', $base);
  } else {
    $base = strtotime(date('Y-m-01', $todayStamp));
    return date('m/'.min(date('t', $base), 30).'/Y', $base);
  }
}

if(  $loan_interest ) {

$installments = ceil((30 / $loan_interest->skip) * $loan_interest->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_decrement = $current_loan->principal;

if( $loan_interest->type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
  $total_interest = ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
} else {
  $interest = (($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip);
  $total_interest = ((($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}

$principal_total = 0;
$interest_total = 0;
$due_increment = 0;

}

?>
<?php $this->load->view('header'); ?>
    <div class="container">

    <?php $this->load->view('members/members_navbar'); ?>

<div class="row">
  <div class="col-md-3 hidden-xs">

 <?php $this->load->view('members/members_loan_sidebar'); ?>

  </div>
  <div class="col-md-9">

<?php if(  $loan_interest ) { ?> 

    <ul class="nav nav-tabs" style="margin-bottom:10px;">

      

      <li role="presentation" class="active"><a href="<?php echo site_url('members/loan/' . $member->id . '/schedule/' . $current_loan->id  ); ?>">Schedule</a></li>
      <?php if( hasAccess('payments', 'view') ) { ?>
      <li role="presentation"><a href="<?php echo site_url('members/loan/' . $member->id . '/payments/' . $current_loan->id  ); ?>">Payments</a></li>
      <?php } ?>
      <li role="presentation" class="visible-xs"><a href="<?php echo site_url('members/loan/' . $member->id . '/details/' . $current_loan->id  ); ?>">Details</a></li>
    </ul>

     <div class="panel panel-default">
        <div class="panel-heading">



         <a href="<?php echo site_url('members/loan/' . $member->id . '/print_schedule/' . $current_loan->id ); ?>" class="btn btn-warning btn-xs pull-right hidden-xs" style="margin-left:5px" target="_print">Print Schedule</a>

        <h3 class="panel-title">Payment Schedule</h3>
        </div>
        <div class="panel-body hidden-xs">

        <table class="table table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="1%">#</th>
              <th class="text-center">Date</th>
              <th class="text-center" colspan="2">Principal</th>
               <th class="text-right">Interest</th>
               <th class="text-center" colspan="3">Amount Due</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-center"></td>
              <td></td>
              <td></td>
              <td class="text-right"><?php echo number_format($principal,2); ?></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="text-right"><?php echo number_format($principal_interest,2); ?></td>
             
            </tr>
<?php 
for($i=1;$i<=$installments;$i++) { 

$principal_total += $current_loan->principal/$installments;
$principal_amount = ($current_loan->principal/$installments);
$principal_decrement -= ($current_loan->principal/$installments);
if( $loan_interest->type == 'diminishing' ) {
  $interest = ((($principal_diminishing * $loan_interest->interest_rate) / 100) * (1/ceil((30 / $loan_interest->skip))));
}
$interest_total += $interest;
$due_amount = $principal_amount+$interest;
$due_increment += $due_amount;
$principal_interest -= $due_amount;
$principal_diminishing -= ($current_loan->principal/$installments);

?>
            <tr class="<?php 
$due_already = '';            
if( time() > strtotime($payment_date) ) {
  $due_already = 'danger'; 
}
            echo ($current_loan->total_payments > $due_increment) ? 'success' : $due_already; 
            ?>">
              <td class="text-center"><?php echo $i; ?></td>
              <td class="text-center"><?php echo $payment_date; ?></td>
              <td class="bold text-right"><?php echo number_format($principal_amount,2); ?></td>
              <td class="text-right"><?php echo number_format($principal_decrement,2); ?></td>
              <td class="bold text-right"><?php echo number_format($interest,2); ?></td>
              <td class="bold text-right"><?php echo number_format($due_amount,2); ?></td>
              <td class="text-right"><?php echo number_format($due_increment,2); ?></td>
              <td class="text-right"><?php echo number_format($principal_interest,2); ?>
              </td>
            </tr>
<?php 
$payment_date = nextPaymentDate( $payment_date ); //date('m/d/Y', (strtotime($payment_date . " + " . $loan_interest->skip ." days")) );
} ?>
            <tr>
              <td class="text-center"></td>
              <td></td>
              <td class="bold text-right"><?php echo number_format($principal_total,2); ?></td>
              <td></td>
              <td class="bold text-right"><?php echo number_format($interest_total,2); ?></td>
              <td class="bold text-right"><?php echo number_format($due_increment,2); ?></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>

        </div>

<div class="list-group visible-xs">
<?php 
$total_due = 0;
$principal_diminishing = $current_loan->principal;
for($i=1;$i<=$installments;$i++) { 
$principal_amount = ($current_loan->principal / $installments);
if( $loan_interest->type == 'diminishing' ) {
  $interest = ((($principal_diminishing * $loan_interest->interest_rate) / 100) * (1/ceil((30 / $loan_interest->skip))));
}
$due_amount = $principal_amount + $interest;
$total_due += $due_amount;
$principal_diminishing -= ( $current_loan->principal/ $installments);
?>
          <div class="list-group-item">
            <span class="font110p bold pull-right"><?php echo number_format($due_amount,2); ?></span>
              <span><?php echo $i; ?></span> 
            </div>
        <?php } ?>
        </div>

        <div class="panel-footer visible-xs">
        <span class="pull-right bold font120p"><?php echo number_format($total_due,2); ?></span>
            <span>TOTAL</span> 
        </div>

      </div>


<?php } else { ?>

<center>
<button class="btn btn-success" data-toggle="modal" data-target="#addInterest">Add Interest</button>
</center>
<br><br>
<?php } ?>

  </div>
</div>  

<?php $this->load->view('members/members_loan_modal'); ?>

    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>