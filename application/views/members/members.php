<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">

 <div class="row">
      
       <div class="col-xs-3 col-sm-3 col-md-3 hidden-xs">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Inactive Members</h3>
        </div>
        <div class="panel-body text-center stat-font">
          <a href="<?php echo site_url('members/inactive'); ?>">
            <?php echo $stats->inactive; ?>
          </a>
        </div>
      </div>
      </div>

      <div class="col-xs-3 col-sm-3 col-md-3 hidden-xs">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Shareholders</h3>
        </div>
        <div class="panel-body text-center stat-font">
          <a href="<?php echo site_url('members/index/shareholder'); ?>">
            <?php echo $stats->shareholders; ?>
          </a>
        </div>
      </div>
      </div>

      <div class="col-xs-3 col-sm-3 col-md-3 hidden-xs">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Lenders</h3>
        </div>
        <div class="panel-body text-center stat-font">
        <a href="<?php echo site_url('members/index/lender'); ?>">
          <?php echo $stats->lenders; ?>
        </a>
        </div>
      </div>
      </div>

      <div class="col-xs-3 col-sm-3 col-md-3 hidden-xs">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">All Active Members</h3>
        </div>
        <div class="panel-body text-center stat-font">
        <a href="<?php echo site_url('members'); ?>">
          <?php echo $stats->members; ?>
        </a>
        </div>
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 visible-xs">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Membership</h3>
        </div>
        <div class="panel-body">
        <table class="table table-default">
          <tr>
            <td>Shareholders</td>
            <td class="text-right"><a href="<?php echo site_url('members/index/shareholder'); ?>"><?php echo $stats->shareholders; ?></a></td>
          </tr>
          <tr>
            <td>Lenders</td>
            <td class="text-right"><a href="<?php echo site_url('members/index/lender'); ?>"><?php echo $stats->lenders; ?></a></td>
          </tr>
          <tr>
            <td>All Members</td>
            <td class="text-right"><a href="<?php echo site_url('members'); ?>"><?php echo $stats->members; ?></a></td>
          </tr>
        </table>

         
        </div>
      </div>
      </div>

</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
<?php if( hasAccess('members', 'add') ) { ?>
        <a href="<?php echo site_url('members/add'); ?>" class="btn btn-success btn-xs pull-right">Add a Member</a>
<?php } ?>
        <h3 class="panel-title">All Active Members</h3>
      </div>
      <div class="panel-body hidden-xs">
        <div class="responsive">
            <table class="table table-striped ">
                <thead>
                  <tr>
                    <th width="50%">Name</th>
                    <th width="50%">Type</th>
                    <th width="100px">Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach( $members as $member ) { ?>
                <tr>
                    <td><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?> 

                    </td>
                    <td><?php echo ucwords($member->member_type); ?></td>
                    <td>
                      <a href="<?php echo site_url("members/profile/".$member->id); ?>" class="btn btn-warning btn-xs">View Member</a>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

        </div>

      </div>

<div class="list-group visible-xs">
        <?php foreach( $members as $member ) { ?>
          <a href="<?php echo site_url("members/profile/".$member->id); ?>" class="list-group-item text-center">
              <strong><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></strong>
              <p><small><?php echo ucwords($member->member_type); ?></small></p>
          </a>
        <?php } ?>
        
<?php echo ($pagination!='') ? '<div class="list-group-item text-center">' . $pagination . '</div>' : ''; ?>
        
        </div>

    </div>
  </div>
</div>
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>