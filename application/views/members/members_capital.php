<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">

<?php $this->load->view('members/members_navbar'); ?>

<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading">
<?php if( hasAccess('capitals', 'add') ) { ?>
       <a href="<?php echo site_url('members/capital/' . $member->id . "/add" ); ?>" class="btn btn-success btn-xs pull-right">Add Capital</a>
<?php } ?>
        <h3 class="panel-title">Contributed Capital</h3>
      </div>
      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>ID</th>
              <th>Date Contributed</th>
              <th>Amount</th>
<?php if( hasAccess('capitals', 'edit') ) { ?>
              <th width="5%">Action</th>
<?php } ?>
            </tr>
          </thead>
          <tbody>
          <?php foreach($capitals as $capital) { ?>
            <tr>
              <td><?php echo $capital->id; ?></td>
              <td><?php echo date('F d, Y', strtotime($capital->date_contributed)); ?></td>
              <td><?php echo number_format($capital->amount,2); ?></td>
<?php if( hasAccess('capitals', 'edit') ) { ?>
              <td>
              <a href="<?php echo site_url( "members/capital/" . $member->id . "/update/" . $capital->id ); ?>" class="btn btn-warning btn-xs">View Capital</a>
              </td>
<?php } ?>
            </tr>
          <?php } ?>
          <tr>
              <th></th>
              <th></th>
              <th><?php echo number_format($member->total_capital,2); ?></th>
<?php if( hasAccess('capitals', 'edit') ) { ?>
              <th></th>
<?php } ?>
            </tr>
          </tbody>
        </table>

      </div>

           <div class="list-group visible-xs">
        <?php foreach($capitals as $capital) { ?>
          <a href="<?php echo site_url( "members/capital/" . $member->id . "/update/" . $capital->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format($capital->amount,2); ?></span>
              <p class="allcaps font70p">Date: <?php echo date('F d, Y', strtotime($capital->date_contributed)); ?> &middot; OR#<?php echo $capital->receipt_number; ?></p>
          </a>
        <?php } ?>
        </div>

 <div class="panel-footer visible-xs">
  <span class="pull-right bold font120p"><?php echo number_format($member->total_capital,2); ?></span>
      <span>TOTAL</span> 
    </div>
    </div>
   
  </div>
</div>  
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>