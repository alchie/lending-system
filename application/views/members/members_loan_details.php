<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

if(  $loan_interest ) {

$installments = ceil((30 / $loan_interest->skip) * $loan_interest->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_total = 0;
$principal_decrement = $current_loan->principal;

if( $loan_interest->type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
  $total_interest = ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
} else {
  $interest = (($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip);
  $total_interest = ((($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}

$interest_total = 0;
$due_increment = 0;

}

?>
<?php $this->load->view('header'); ?>
    <div class="container">

    <?php $this->load->view('members/members_navbar'); ?>

<div class="row">
  <div class="col-md-12">

<?php if(  $loan_interest ) { 

$balance =  $principal_interest; 

?> 

    <ul class="nav nav-tabs" style="margin-bottom:10px;">
      <li role="presentation"><a href="<?php echo site_url('members/loan/' . $member->id . '/view/' . $current_loan->id  ); ?>">Payments</a></li>
      <li role="presentation"><a href="<?php echo site_url('members/loan/' . $member->id . '/schedule/' . $current_loan->id  ); ?>">Schedule</a></li>
      <li role="presentation" class="visible-xs active"><a href="<?php echo site_url('members/loan/' . $member->id . '/details/' . $current_loan->id  ); ?>">Details</a></li>
    </ul>

       

<div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') ) { ?>
        <a href="<?php echo site_url('members/loan/' . $member->id . '/edit/' . $current_loan->id  ); ?>" class="btn btn-warning btn-xs pull-right">Update</a>
<?php } ?>
        <h3 class="panel-title">Principal</h3>
        </div>
        <div class="panel-body">

        <div class="form-group">
            <label class="control-label">Loan Date</label>
            <div class="form-control text-right"><?php echo date('m/d/Y', strtotime($current_loan->loan_date)); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Principal Amount</label>
            <div class="form-control text-right"><?php echo number_format($current_loan->principal,2); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Payment Start</label>
            <div class="form-control text-right"><?php echo date('m/d/Y', strtotime($current_loan->payment_start)); ?></div>
        </div>

        </div>
</div>

<?php if(  $loan_interest ) { ?>

<div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') ) { ?>
        <a href="<?php echo site_url('members/loan/' . $member->id . '/interest/' . $current_loan->id  ); ?>" class="btn btn-warning btn-xs pull-right">Update</a>
<?php } ?>
        <h3 class="panel-title">Interests</h3>
        </div>
        <div class="panel-body">

         <div class="form-group">
            <label class="control-label">Interest Rate and Type</label>
            <div class="form-control text-right"><?php echo $loan_interest->interest_rate; ?>% Monthly (<em><?php echo ucwords($loan_interest->type); ?></em>)</div>
        </div>

        <div class="form-group">
            <label class="control-label">Total Addon Interest</label>
            <div class="form-control text-right"><?php echo number_format($total_interest,2); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Installments</label>
            <div class="form-control text-right"><?php echo $installments; ?></div>
        </div>

        </div>
</div>

<?php } ?>


       
<?php } else { ?>

<center>
<a href="<?php echo site_url("members/loan/{$member->id}/interest/{$current_loan->id}"); ?>" class="btn btn-success">Add Interest</a>
</center>

  <?php } ?>

  </div>

  </div>


</div>  
</div>  

<?php $this->load->view('footer'); ?>