<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

if(  $loan_interest ) {

$installments = ceil((30 / $loan_interest->skip) * $loan_interest->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_total = 0;
$principal_decrement = $current_loan->principal;

if( $loan_interest->type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
  $total_interest = ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
} else {
  $interest = (($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip);
  $total_interest = ((($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}

$interest_total = 0;
$due_increment = 0;

}

?>
<?php $this->load->view('header'); ?>
    <div class="container">

    <?php $this->load->view('members/members_navbar'); ?>

<div class="row">
  <div class="col-md-3 hidden-xs">

<?php $this->load->view('members/members_loan_sidebar'); ?>

  </div>
  <div class="col-md-9">

<?php if(  $loan_interest ) { 

$balance =  $principal_interest; 

?> 

    <ul class="nav nav-tabs" style="margin-bottom:10px;">
      <li role="presentation"><a href="<?php echo site_url('members/loan/' . $member->id . '/schedule/' . $current_loan->id  ); ?>">Schedule</a></li>
<?php if( hasAccess('payments', 'view') ) { ?>
      <li role="presentation" class="active"><a href="<?php echo site_url('members/loan/' . $member->id . '/payments/' . $current_loan->id  ); ?>">Payments</a></li>
<?php } ?>
      <li role="presentation" class="visible-xs"><a href="<?php echo site_url('members/loan/' . $member->id . '/details/' . $current_loan->id  ); ?>">Details</a></li>
    </ul>

        <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') ) { ?>
        <a href="<?php echo site_url('members/loan/' . $member->id . '/payment/' . $current_loan->id ); ?>" class="btn btn-success btn-xs pull-right">Add Payment</a>
<?php } ?>
        <h3 class="panel-title">Payments</h3>
        </div>
        <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="10%">Receipt #</th>
              <th class="text-center">Payment Date</th>
              <th class="text-right">Amount</th>
              <th class="text-right">Balance</th>
<?php if( hasAccess('payments', 'edit') ) { ?>
              <th class="text-right" width="70px"></th>
<?php } ?>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td class="text-center"></td>
              <td class="text-center"></td>
              <td class="text-right"></td>
              <td class="text-right"><?php echo number_format($balance,2); ?></td>
<?php if( hasAccess('payments', 'edit') ) { ?>
              <td></td>
<?php } ?>
            </tr>
<?php foreach($payments as $payment) { 
  $balance -= $payment->amount;
?>
            <tr>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-center"><?php echo $payment->payment_date; ?></td>
              <td class="text-right bold"><?php echo number_format($payment->amount,2); ?></td>
              <td class="text-right"><?php echo number_format($balance,2); ?></td>
<?php if( hasAccess('payments', 'edit') ) { ?>
              <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("members/loan/{$member->id}/payment_update/{$payment->id}"); ?>">Update</a></td>
<?php } ?>
            </tr>
          <?php } ?>
          </tbody>
          </table>

        </div>

        <div class="list-group visible-xs">
        <?php foreach($payments as $payment) {  ?>
          <a href="<?php echo site_url("members/loan/{$member->id}/payment_update/{$payment->id}"); ?>" class="list-group-item text-center">
              <span class="font120p bold"><?php echo number_format($payment->amount,2); ?></span>
              <p class="allcaps font70p">Date: <?php echo date('F d, Y', strtotime($payment->payment_date)); ?> &middot; OR#<?php echo $payment->receipt_number; ?></p>
          </a>
        <?php } ?>
        </div>

        <div class="panel-footer visible-xs">
          <span class="pull-right font120p bold"><?php echo number_format($balance,2); ?></span>
          <span>BALANCE:</span> 
        </div>
      </div>
<?php } else { ?>

<center>
<a href="<?php echo site_url("members/loan/{$member->id}/add_interest/{$current_loan->id}"); ?>" class="btn btn-success">Add Interest</a>
</center>

  <?php } ?>

  </div>

  </div>


</div>  
</div>  

<?php $this->load->view('members/members_loan_modal'); ?>

<?php $this->load->view('footer'); ?>