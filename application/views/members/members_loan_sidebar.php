 <?php 

if(  $loan_interest ) {

$installments = ceil((30 / $loan_interest->skip) * $loan_interest->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_decrement = $current_loan->principal;

if( $loan_interest->type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
  $total_interest = ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
} else {
  $interest = (($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip);
  $total_interest = ((($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}

$principal_total = 0;
$interest_total = 0;
$due_increment = 0;

}

 ?>

<?php if( hasAccess('loans', 'edit') && ($current_loan->status=='undecided') ) { ?>
    <div class="panel panel-success">
      <div class="panel-heading">Loan Status</div>
      <div class="panel-body">
        <a class="btn btn-block btn-success" href="<?php echo site_url('members/loan/' . $member->id . '/approve/' . $current_loan->id  ); ?>">Approve</a>
        <a class="btn btn-block btn-danger" href="<?php echo site_url('members/loan/' . $member->id . '/disapprove/' . $current_loan->id  ); ?>">Disapprove</a>
      </div>
    </div>

<?php } ?>

 <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') || ( hasAccess('loans', 'add') && ($current_loan->submitted_by == $this->session->member_id) && ($current_loan->status=='undecided') ) ) { ?>
        <button class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateLoan">Update</button>
<?php } ?>
        <h3 class="panel-title">Principal</h3>
        </div>
        <div class="panel-body">

        <div class="form-group">
            <label class="control-label">Loan Date</label>
            <div class="form-control text-right"><?php echo date('m/d/Y', strtotime($current_loan->loan_date)); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Principal Amount</label>
            <div class="form-control text-right"><?php echo number_format($current_loan->principal,2); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Payment Start</label>
            <div class="form-control text-right"><?php echo date('m/d/Y', strtotime($current_loan->payment_start)); ?></div>
        </div>

        </div>
</div>

<?php if(  $loan_interest ) { ?>

<div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') || ( hasAccess('loans', 'add') && ($current_loan->submitted_by == $this->session->member_id) && ($current_loan->status=='undecided') ) ) { ?>
        <button data-toggle="modal" data-target="#addInterest" class="btn btn-warning btn-xs pull-right">Update</button>
<?php } ?>
        <h3 class="panel-title">Interests</h3>
        </div>
        <div class="panel-body">

         <div class="form-group">
            <label class="control-label">Interest Rate and Type</label>
            <div class="form-control text-right"><?php echo $loan_interest->interest_rate; ?>% Monthly (<em><?php echo ucwords($loan_interest->type); ?></em>)</div>
        </div>

        <div class="form-group">
            <label class="control-label">Total Addon Interest</label>
            <div class="form-control text-right"><?php echo number_format($total_interest,2); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label"># of Months</label>
            <div class="form-control text-right"><?php echo $loan_interest->months; ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Installments</label>
            <div class="form-control text-right"><?php echo $installments; ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Effective Interest Rate</label>
            <div class="form-control text-right"><?php echo number_format( (($total_interest / $current_loan->principal) * 100),2); ?>%</div>
        </div>

        </div>
</div>

<?php } ?>
