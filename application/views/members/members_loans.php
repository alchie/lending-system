<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">

<?php $this->load->view('members/members_navbar'); ?>

<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading">
<?php if( hasAccess('loans', 'add') ) { ?>
       <button class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#addLoan">Add Loan</button>
<?php } ?>
        <h3 class="panel-title">Loans</h3>
      </div>
      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>Date</th>
              <th>Principal</th>
              <th>Interest</th>
              <th>Interest Type</th>
              <th>Paid</th>
              <th>Balance</th>
              <th width="4%">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($loans as $loan) { 

            if($loan->status=='disapproved') {
              continue;
            }
            
$interest_rate = ($loan->interest_rate) ? $loan->interest_rate : 0;
$skip = ($loan->skip) ? $loan->skip : 1;
$installments = ceil((30 / $skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 

if( $loan->type == 'diminishing' ) {
  $principal_interest = $loan->principal + ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
  $total_interest = ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
} else {
  $interest = (($loan->principal * $interest_rate) / 100) / ceil(30 / $skip);
  $total_interest = ((($loan->principal * $interest_rate) / 100) / ceil(30 / $skip) * $installments);
  $principal_interest = $loan->principal + ($interest * $installments);
}

            ?>
            <tr class="<?php echo ($loan->status=='undecided')?'danger':''; ?>">
              <td><?php echo date('m/d/Y', strtotime($loan->loan_date)); ?></td>
              <td><?php echo number_format($loan->principal,2); ?></td>
              <td><?php echo number_format($total_interest,2); ?></td>
              <td><?php echo ucwords($loan->type); ?></td>
              <td><?php echo number_format($loan->payments,2); ?></td>
              <td><?php echo number_format(($loan->principal + $total_interest) - $loan->payments,2); ?></td>
              <td><a href="<?php echo site_url( "members/loan/" . $member->id . "/schedule/" . $loan->id ); ?>" class="btn btn-warning btn-xs">View Loan</a></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>

      </div>

       <div class="list-group visible-xs">
        <?php foreach($loans as $loan) {  ?>
          <a href="<?php echo site_url( "members/loan/" . $member->id . "/schedule/" . $loan->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format($loan->principal,2); ?></span>
              <p class="allcaps font70p">Date: <?php echo date('F d, Y', strtotime($loan->loan_date)); ?></p>
          </a>
        <?php } ?>
        </div>

    </div>

  </div>
</div>  

<!-- #addLoan Modal -->
<div class="modal fade" id="addLoan" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Loan</h4>
      </div>
<form method="post" action="<?php echo site_url("members/loan/{$member->id}/add"); ?>">
    <div class="modal-body">
        <div class="form-group <?php echo (form_error('loan_date')) ? 'has-error' : ''; ?>">
            <label class="control-label">Loan Date</label>
            <input class="form-control datepicker" type="text" name="loan_date" value="<?php echo ($this->input->post('loan_date')) ? $this->input->post('loan_date') : date('m/d/Y'); ?>">
        </div>
        <div class="form-group <?php echo (form_error('principal')) ? 'has-error' : ''; ?>">
            <label class="control-label">Principal Amount</label>
            <input class="form-control" type="text" name="principal" value="<?php echo $this->input->post('principal'); ?>">
        </div>
        <div class="form-group <?php echo (form_error('payment_start')) ? 'has-error' : ''; ?>">
            <label class="control-label">Payment Start Date</label>
            <input class="form-control datepicker" type="text" name="payment_start" value="<?php echo $this->input->post('payment_start'); ?>">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add Loan</button>
      </div>
</form>
    </div>
  </div>
</div>

    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>