<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <?php $this->load->view('members/members_navbar'); ?>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add Interest</h3>
      </div>
      <form method="post">
      <div class="panel-body">
      <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
        <div class="form-group">
            <label class="control-label">Interest Rate</label>
            <select class="form-control" name="interest_rate">
            <?php 
            $interest_rate = ($loan_interest->interest_rate) ? $loan_interest->interest_rate : 3;
            for($i=1;$i<=10;$i++) {
                echo '<option value="'.$i.'" '.(($i==$interest_rate)?'selected':'').'>'.$i.'</option>';
                if( $i < 10) {
                echo '<option value="'.($i+0.50).'" '.((($i+0.50)==$interest_rate)?'selected':'').'>'.($i+0.50).'</option>';
                }
            } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Number of Months</label>
             <select class="form-control" name="months">
            <?php 
            $months = ($loan_interest->months) ? $loan_interest->months : 12;
            for($i=1;$i<=24;$i++) {
                echo '<option value="'.$i.'" '.(($i==$months)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Payment Skip Days</label>
            <select class="form-control" name="skip">
            <?php 
            $skip = ($loan_interest->skip) ? $loan_interest->skip : 15;
            for($i=1;$i<=30;$i++) {
                echo '<option value="'.$i.'" '.(($i==$skip)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Interest Type</label>
            <select class="form-control" name="type">
            <?php foreach(array('fixed'=>'Fixed', 'diminishing'=>'Diminishing') as $key=>$value) { ?>
              <option value="<?php echo $key; ?>" <?php echo (($loan_interest) && ($loan_interest->type == $key)) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
            <?php } ?>
            </select>
        </div>
      </div>
      <div class="panel-footer">
        <input type="submit" class="btn btn-success" value="Submit">
        <a href="javascript:history.back(-1);" class="btn btn-danger">Back</a>
      </div>
      </form>
    </div>
  </div>
</div>  
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>