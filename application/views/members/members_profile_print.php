<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Print Member Data</title>
  <style>
    <!--
    body {
      font-family: sans-serif;
      padding:0;
      margin:0;
    }
    .container {
      width: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      font-size: 10px;
      text-decoration: underline;
    }
    .detail {
      font-size: 15px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: top;
      padding-bottom: 10px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">
  <table width="100%" class="border-bottom">
    <tr>
      <td width="33.33%">
        <span class="label">Last Name</span>
        <div class="detail"><?php echo $member->lastname; ?></div>
      </td>
      <td width="33.33%">
        <span class="label">First Name</span>
        <div class="detail"><?php echo $member->firstname; ?></div>
      </td>
      <td width="33.33%">
        <span class="label">Middle Name</span>
        <div class="detail"><?php echo $member->middlename; ?></div>
      </td>
    </tr>
  </table>
  <table width="100%" class="border-bottom">
    <tr>
      <td width="25%">
        <span class="label">Birthday</span>
         <div class="detail"><?php echo date('F d, Y', strtotime($member->birthdate)); ?></div>
      </td>
      <td width="25%">
        <span class="label">Birth Place</span>
        <div class="detail"><?php echo $member->birthplace; ?></div>
      </td>
      <td width="25%">
        <span class="label">Gender</span>
        <div class="detail"><?php echo ucwords($member->gender); ?></div>
      </td>
       <td width="25%">
        <span class="label">Marital Status</span>
        <div class="detail"><?php echo ucwords($member->marital_status); ?></div>
      </td>
    </tr>
  </table>
  <table width="100%" class="border-bottom">
    <tr>
      <td>
        <span class="label">Address</span>
        <div class="detail"><?php echo ($member->unit) ? $member->unit . "," : ""; ?> <?php echo ($member->building) ? $member->building. "," : ""; ?> <?php echo ($member->lot_block) ? $member->lot_block. "," : ""; ?> <?php echo ($member->street) ? $member->street. "," : ""; ?> <?php echo ($member->subdivision) ? $member->subdivision. "," : ""; ?> <?php echo ($member->barangay) ? $member->barangay. "," : ""; ?> <?php echo ($member->city) ? $member->city. "," : ""; ?> <?php echo ($member->province) ? $member->province. "," : ""; ?> <?php echo $member->zip; ?></div>
      </td>
    </tr>
  </table>
  <table width="100%" class="border-bottom">
    <tr>
      <td width="25%">
        <span class="label">Mobile Phone Number</span>
        <div class="detail"><?php echo $member->phone_mobile; ?></div>
      </td>
      <td width="25%">
        <span class="label">Home Phone Number</span>
        <div class="detail"><?php echo $member->phone_home; ?></div>
      </td>
      <td width="25%">
        <span class="label">Office Phone Number</span>
        <div class="detail"><?php echo $member->phone_office; ?></div>
      </td>
       <td width="25%">
        <span class="label">Email Address</span>
        <div class="detail"><?php echo $member->email; ?></div>
      </td>
    </tr>
  </table>
</div> 

</body>
</html>