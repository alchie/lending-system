<?php if( hasAccess('loans', 'edit') || ( hasAccess('loans', 'add') && ($current_loan->submitted_by == $this->session->member_id) && ($current_loan->status=='undecided') ) ) { ?>

<!-- #addInterest Modal -->
<div class="modal fade" id="addInterest" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Interest</h4>
      </div>
<form method="post" action="<?php echo site_url("members/loan/{$member->id}/update_interest/{$current_loan->id}"); ?>">
    <div class="modal-body">

<div class="form-group">
            <label class="control-label">Interest Rate</label>
            <select class="form-control" name="interest_rate">
            <?php 
            echo '<option value="'.(0.50).'" '.(((0.50)==$interest_rate)?'selected':'').'>'.number_format((0.50),2).'</option>';
            $interest_rate = ($loan_interest->interest_rate) ? $loan_interest->interest_rate : 3;
            for($i=1;$i<=10;$i++) {
                echo '<option value="'.$i.'" '.(($i==$interest_rate)?'selected':'').'>'.number_format($i,2).'</option>';
                if( $i < 10) {
                echo '<option value="'.($i+0.50).'" '.((($i+0.50)==$interest_rate)?'selected':'').'>'.number_format(($i+0.50),2).'</option>';
                }
            } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Number of Months</label>
             <select class="form-control" name="months">
            <?php 
            $months = ($loan_interest->months) ? $loan_interest->months : 12;
            for($i=1;$i<=24;$i++) {
                echo '<option value="'.$i.'" '.(($i==$months)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Payment Skip Days</label>
            <select class="form-control" name="skip">
            <?php 
            $skip = ($loan_interest->skip) ? $loan_interest->skip : 15;
            for($i=1;$i<=30;$i++) {
                echo '<option value="'.$i.'" '.(($i==$skip)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Interest Type</label>
            <select class="form-control" name="type">
            <?php foreach(array('fixed'=>'Fixed', 'diminishing'=>'Diminishing') as $key=>$value) { ?>
              <option value="<?php echo $key; ?>" <?php echo (($loan_interest) && ($loan_interest->type == $key)) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
            <?php } ?>
            </select>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
    </div>
  </div>
</div>

<!-- #updateLoan Modal -->
<div class="modal fade" id="updateLoan" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Loan</h4>
      </div>
<form method="post" action="<?php echo site_url("members/loan/{$member->id}/edit/{$current_loan->id}"); ?>">
    <div class="modal-body">
        <div class="form-group <?php echo (form_error('loan_date')) ? 'has-error' : ''; ?>">
            <label class="control-label">Loan Date</label>
            <input class="form-control datepicker" type="text" name="loan_date" value="<?php echo date('m/d/Y', strtotime($current_loan->loan_date)); ?>">
        </div>
        <div class="form-group <?php echo (form_error('principal')) ? 'has-error' : ''; ?>">
            <label class="control-label">Principal Amount</label>
            <input class="form-control" type="text" name="principal" value="<?php echo $current_loan->principal; ?>">
        </div>
        <div class="form-group <?php echo (form_error('payment_start')) ? 'has-error' : ''; ?>">
            <label class="control-label">Payment Start Date</label>
            <input class="form-control datepicker" type="text" name="payment_start" value="<?php echo date('m/d/Y', strtotime($current_loan->payment_start)); ?>">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php } ?>