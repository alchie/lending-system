<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );

?>
    <div class="container">
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add a Member</h3>
      </div>
      <form method="post">
      <div class="panel-body">
      <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
        <div class="form-group <?php echo (form_error('lastname')) ? 'has-error' : ''; ?>">
            <label class="control-label">Last Name</label>
            <input class="form-control" type="text" name="lastname" value="<?php echo $this->input->post('lastname'); ?>">
        </div>
        <div class="form-group <?php echo (form_error('firstname')) ? 'has-error' : ''; ?>">
            <label class="control-label">First Name</label>
            <input class="form-control" type="text" name="firstname" value="<?php echo $this->input->post('firstname'); ?>">
        </div>
        <div class="form-group <?php echo (form_error('middlename')) ? 'has-error' : ''; ?>">
            <label class="control-label">Middle Name</label>
            <input class="form-control" type="text" name="middlename" value="<?php echo $this->input->post('middlename'); ?>">
        </div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Birthday</label>
           <input type="text" class="form-control datepicker" name="birthdate" value="">
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
             <label class="control-label">Birth Place</label>
           <input type="text" class="form-control" name="birthplace" value="">
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Gender</label>
            <select class="form-control" name="gender">
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Marital Status</label>
            <select class="form-control" name="marital_status">
            <?php foreach($marital_statuses as $key=>$value) { ?>
              <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
              <?php } ?>
            </select>
        </div>
  </div>
</div>

      </div>
      <div class="panel-footer">
        <input type="submit" class="btn btn-success" value="Submit">
        <a href="javascript:history.back(-1);" class="btn btn-danger">Back</a>
      </div>
      </form>
    </div>
  </div>
</div>  
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>