<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">

    <?php $this->load->view('members/members_navbar'); ?>
    
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><strong>Add Loan</h3>
      </div>
      <form method="post">
      <div class="panel-body">
      <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
        <div class="form-group <?php echo (form_error('loan_date')) ? 'has-error' : ''; ?>">
            <label class="control-label">Loan Date</label>
            <input class="form-control datepicker" type="text" name="loan_date" value="<?php echo ($this->input->post('loan_date')) ? $this->input->post('loan_date') : date('m/d/Y'); ?>">
        </div>
        <div class="form-group <?php echo (form_error('principal')) ? 'has-error' : ''; ?>">
            <label class="control-label">Principal Amount</label>
            <input class="form-control" type="text" name="principal" value="<?php echo $this->input->post('principal'); ?>">
        </div>
        <div class="form-group <?php echo (form_error('payment_start')) ? 'has-error' : ''; ?>">
            <label class="control-label">Payment Start Date</label>
            <input class="form-control datepicker" type="text" name="payment_start" value="<?php echo $this->input->post('payment_start'); ?>">
        </div>
      </div>
      <div class="panel-footer">
        <input type="submit" class="btn btn-success" value="Submit">
        <a href="<?php echo site_url("members/loan/" . $member->id); ?>" class="btn btn-danger">Back</a>
      </div>
      </form>
    </div>
  </div>
</div>  

    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>