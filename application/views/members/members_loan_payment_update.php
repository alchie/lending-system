<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">

    <?php $this->load->view('members/members_navbar'); ?>
    
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Update Payment</h3>
      </div>
      <form method="post">
      <div class="panel-body">
      <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
      <div class="form-group <?php echo (form_error('receipt_number')) ? 'has-error' : ''; ?>">
            <label class="control-label">Receipt Number</label>
            <input class="form-control" type="text" name="receipt_number" value="<?php echo $payment->receipt_number; ?>">
        </div>
        <div class="form-group <?php echo (form_error('payment_date')) ? 'has-error' : ''; ?>">
            <label class="control-label">Payment Date</label>
            <input class="form-control datepicker" type="text" name="payment_date" value="<?php echo date('m/d/Y', strtotime($payment->payment_date)); ?>">
        </div>
        <div class="form-group <?php echo (form_error('amount')) ? 'has-error' : ''; ?>">
            <label class="control-label">Amount Paid</label>
            <input class="form-control" type="text" name="amount" value="<?php echo $payment->amount; ?>">
        </div>
      </div>
      <div class="panel-footer">
        <input type="submit" class="btn btn-success" value="Submit">
        <a href="<?php echo site_url('members/loan/' . $member->id . '/view/' . $payment->loan_id); ?>" class="btn btn-danger">Back</a>
      </div>
      </form>
    </div>
  </div>
</div>  
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>