<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">Lending</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

<?php if( hasAccess('members', 'view') ) { ?>
       <form class="navbar-form navbar-left" role="search" action="<?php echo site_url("members"); ?>">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search Member" name="q">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
          </span>
        </div>
      </form>
<?php } ?>
          <ul class="nav navbar-nav">
<?php if( hasAccess('members', 'view') ) { ?>
            <li><a href="<?php echo site_url('members'); ?>">Members</a></li>
<?php } 
if( hasAccess('capitals', 'view') ) { ?>
            <li><a href="<?php echo site_url('capitals'); ?>">Capitals</a></li>
<?php } 
if( hasAccess('loans', 'view') ) { ?>
            <li><a href="<?php echo site_url('loans'); ?>">Loans</a></li>
<?php } ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->firstname; ?> <?php echo $this->session->lastname; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('account/profile'); ?>">My Profile</a></li>
<?php if($this->session->member_type=='shareholder') { ?>
            <li><a href="<?php echo site_url('account/capital'); ?>">My Capital</a></li>
<?php } ?>
            <li><a href="<?php echo site_url('account/loans'); ?>">My Loans</a></li>            
            <li><a href="<?php echo site_url('account/change_password'); ?>">Change Password</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
          </ul>
        </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>