<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">

<div class="row">
  <div class="col-md-12">

<?php $this->load->view('account/account_navbar'); ?>

  </div>
</div>


<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading">

        <h3 class="panel-title">My Loans</h3>
      </div>

      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>Date</th>
              <th>Principal</th>
              <th>Interest</th>
              <th>Paid</th>
              <th>Balance</th>
              <th width="4%">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($loans as $loan) { 
$installments = ceil((30 / $loan->skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 

if( $loan->type == 'diminishing' ) {
  $principal_interest = $loan->principal + ((($loan->principal * ($loan->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan->skip))));
  $total_interest = ((($loan->principal * ($loan->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan->skip))));
} else {
  $interest = (($loan->principal * $loan->interest_rate) / 100) / ceil(30 / $loan->skip);
  $total_interest = ((($loan->principal * $loan->interest_rate) / 100) / ceil(30 / $loan->skip) * $installments);
  $principal_interest = $loan->principal + ($interest * $installments);
}

            ?>
            <tr>
              <td><?php echo date('m/d/Y', strtotime($loan->loan_date)); ?></td>
              <td><?php echo number_format($loan->principal,2); ?></td>
              <td><?php echo number_format($total_interest,2); ?></td>
              <td><?php echo number_format($loan->payments,2); ?></td>
              <td><?php echo number_format(($loan->principal + $total_interest) - $loan->payments,2); ?></td>
              <td><a href="<?php echo site_url( "account/loan/" . $loan->id ); ?>" class="btn btn-warning btn-xs">View Loan</a></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>

      </div>

       <div class="list-group visible-xs">
        <?php foreach($loans as $loan) {  ?>
          <a href="<?php echo site_url( "account/loan/" . $loan->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format($loan->principal,2); ?></span>
              <p class="allcaps font70p">Date: <?php echo date('F d, Y', strtotime($loan->loan_date)); ?></p>
          </a>
        <?php } ?>
        </div>
      
    </div>

  </div>
</div>  


    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>