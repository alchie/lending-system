<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );

?>
<?php $this->load->view('header'); ?>
    <div class="container">



<div class="row">
  <div class="col-md-12">

<?php $this->load->view('account/account_navbar'); ?>

  </div>
</div>


<div class="row">
  <div class="col-md-12">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

    <div class="panel panel-default">
      <div class="panel-heading">

        <h3 class="panel-title">Personal Information</h3>
      </div>

      <div class="panel-body">
            <input type="hidden" name="action" value="personal_info">

<div class="row">
  <div class="col-md-4">
      
        <div class="form-group">
            <label class="control-label">Last Name</label>
            <div class="form-control"><?php echo $member->lastname; ?></div>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">First Name</label>
            <div class="form-control"><?php echo $member->firstname; ?></div>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Middle Name</label>
            <div class="form-control"><?php echo $member->middlename; ?></div>
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Birthday</label>
            <div class="form-control"><?php echo date('F d, Y', strtotime($member->birthdate)); ?></div>
        </div>
  </div>
  <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Birth Place</label>
            <div class="form-control"><?php echo ucwords($member->birthplace); ?></div>
        </div>
  </div>
    <div class="col-md-3">
         <div class="form-group">
            <label class="control-label">Gender</label>
            <div class="form-control"><?php echo ucwords($member->gender); ?></div>
        </div>
  </div>
  <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Marital Status</label>
            <div class="form-control"><?php echo $marital_statuses[$member->marital_status]; ?></div>
        </div>
  </div>
</div>
      </div>
      
    </div>

  </div>
</div>  

<div class="row">
<div class="col-md-12">
    <div class="panel panel-default">
          <div class="panel-heading">

            <h3 class="panel-title">Address</h3>
          </div>
          <div class="panel-body">
<div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Unit / Room No. &amp; Floor</label>
              <div class="form-control"><?php echo ucwords($member->unit); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Building Name</label>
              <div class="form-control"><?php echo ucwords($member->building); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Lot No. &amp; Block No. &amp; Phase No.</label>
              <div class="form-control"><?php echo ucwords($member->lot_block); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Street</label>
              <div class="form-control"><?php echo ucwords($member->street); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Subdivision</label>
              <div class="form-control"><?php echo ucwords($member->subdivision); ?></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Barangay</label>
              <div class="form-control"><?php echo ucwords($member->barangay); ?></div>
            </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Municipality/City</label>
              <div class="form-control"><?php echo ucwords($member->city); ?></div>
            </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Province</label>
              <div class="form-control"><?php echo ucwords($member->province); ?></div>
            </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label">Zip Code</label>
              <div class="form-control"><?php echo ucwords($member->zip); ?></div>
            </div>
          </div>
</div>
          </div>
    </div>
  </div>

</div>
<div class="row">

<div class="col-md-4">
    <div class="panel panel-default">
          <div class="panel-heading">

            <h3 class="panel-title">Contact Information</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label">Mobile Phone Number</label>
              <div class="form-control"><?php echo $member->phone_mobile; ?></div>
            </div>
            <div class="form-group">
              <label class="control-label">Home Phone Number</label>
              <div class="form-control"><?php echo $member->phone_home; ?></div>
            </div>
            <div class="form-group">
              <label class="control-label">Office Phone Number</label>
              <div class="form-control"><?php echo $member->phone_office; ?></div>
            </div>
            <div class="form-group">
              <label class="control-label">Email Address</label>
              <div class="form-control"><?php echo $member->email; ?></div>
            </div>
          </div>
    </div>
  </div>

    

</div>


    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>