<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

if(  $loan_interest ) {

$installments = ceil((30 / $loan_interest->skip) * $loan_interest->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_decrement = $current_loan->principal;

if( $loan_interest->type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
  $total_interest = ((($current_loan->principal * ($loan_interest->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $loan_interest->skip))));
} else {
  $interest = (($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip);
  $total_interest = ((($current_loan->principal * $loan_interest->interest_rate) / 100) / ceil(30 / $loan_interest->skip) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}

$principal_total = 0;
$interest_total = 0;
$due_increment = 0;

}

?>
<?php $this->load->view('header'); ?>
    <div class="container">

<?php $this->load->view('account/account_navbar'); ?>

<div class="row">
  <div class="col-md-3 hidden-xs">

 <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') ) { ?>
        <a href="<?php echo site_url('members/loan/' . $member->id . '/edit/' . $current_loan->id  ); ?>" class="btn btn-warning btn-xs pull-right">Update</a>
<?php } ?>
        <h3 class="panel-title">Principal</h3>
        </div>
        <div class="panel-body">

        <div class="form-group">
            <label class="control-label">Loan Date</label>
            <div class="form-control text-right"><?php echo date('m/d/Y', strtotime($current_loan->loan_date)); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Principal Amount</label>
            <div class="form-control text-right"><?php echo number_format($current_loan->principal,2); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Payment Start</label>
            <div class="form-control text-right"><?php echo date('m/d/Y', strtotime($current_loan->payment_start)); ?></div>
        </div>

        </div>
</div>

<?php if(  $loan_interest ) { ?>

<div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('loans', 'edit') ) { ?>
        <a href="<?php echo site_url(str_replace('schedule','interest',uri_string())); ?>" class="btn btn-warning btn-xs pull-right">Update</a>
<?php } ?>
        <h3 class="panel-title">Interests</h3>
        </div>
        <div class="panel-body">

         <div class="form-group">
            <label class="control-label">Interest Rate and Type</label>
            <div class="form-control text-right"><?php echo $loan_interest->interest_rate; ?>% Monthly (<em><?php echo ucwords($loan_interest->type); ?></em>)</div>
        </div>

        <div class="form-group">
            <label class="control-label">Total Addon Interest</label>
            <div class="form-control text-right"><?php echo number_format($total_interest,2); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Installments</label>
            <div class="form-control text-right"><?php echo $installments; ?></div>
        </div>

        </div>
</div>

<?php } ?>

  </div>
  <div class="col-md-9">

<?php if(  $loan_interest ) { ?> 

    <ul class="nav nav-tabs" style="margin-bottom:10px;">
      <li role="presentation"><a href="<?php echo site_url( 'account/loan/' . $current_loan->id . "/payments"  ); ?>">Payments</a></li>
      <li role="presentation" class="active"><a href="<?php echo site_url( 'account/loan/' . $current_loan->id . "/schedule"  ); ?>">Schedule</a></li>
      <li role="presentation" class="visible-xs"><a href="<?php echo site_url( 'account/loan/' . $current_loan->id . "/details"  ); ?>">Details</a></li>
    </ul>

     <div class="panel panel-default">
        <div class="panel-heading">

        <h3 class="panel-title">Payment Schedule</h3>
        </div>
        <div class="panel-body hidden-xs">

        <table class="table table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="1%">#</th>
              <th class="text-center">Date</th>
              <th class="text-center" colspan="2">Principal</th>
               <th class="text-right">Interest</th>
               <th class="text-center" colspan="3">Amount Due</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-center"></td>
              <td></td>
              <td></td>
              <td class="text-right"><?php echo number_format($principal,2); ?></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="text-right"><?php echo number_format($principal_interest,2); ?></td>
             
            </tr>
<?php 
for($i=1;$i<=$installments;$i++) { 

$principal_total += $current_loan->principal/$installments;
$principal_amount = ($current_loan->principal/$installments);
$principal_decrement -= ($current_loan->principal/$installments);
if( $loan_interest->type == 'diminishing' ) {
  $interest = ((($principal_diminishing * $loan_interest->interest_rate) / 100) * (1/ceil((30 / $loan_interest->skip))));
}
$interest_total += $interest;
$due_amount = $principal_amount+$interest;
$due_increment += $due_amount;
$principal_interest -= $due_amount;
$payment_date = date('m/d/Y', (strtotime($payment_date . " + " . $loan_interest->skip ." days")) );
$principal_diminishing -= ($current_loan->principal/$installments);

?>
            <tr class="<?php echo ($current_loan->total_payments > $due_increment) ? 'success' : ''; ?>">
              <td class="text-center"><?php echo $i; ?></td>
              <td class="text-center"><?php echo $payment_date; ?></td>
              <td class="bold text-right"><?php echo number_format($principal_amount,2); ?></td>
              <td class="text-right"><?php echo number_format($principal_decrement,2); ?></td>
              <td class="bold text-right"><?php echo number_format($interest,2); ?></td>
              <td class="bold text-right"><?php echo number_format($due_amount,2); ?></td>
              <td class="text-right"><?php echo number_format($due_increment,2); ?></td>
              <td class="text-right"><?php echo number_format($principal_interest,2); ?>
              </td>
            </tr>
<?php } ?>
            <tr>
              <td class="text-center"></td>
              <td></td>
              <td class="bold text-right"><?php echo number_format($principal_total,2); ?></td>
              <td></td>
              <td class="bold text-right"><?php echo number_format($interest_total,2); ?></td>
              <td class="bold text-right"><?php echo number_format($due_increment,2); ?></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>

        </div>

<div class="list-group visible-xs">
<?php 
$total_due = 0;
$principal_diminishing = $current_loan->principal;
for($i=1;$i<=$installments;$i++) { 
$principal_amount = ($current_loan->principal / $installments);
if( $loan_interest->type == 'diminishing' ) {
  $interest = ((($principal_diminishing * $loan_interest->interest_rate) / 100) * (1/ceil((30 / $loan_interest->skip))));
}
$due_amount = $principal_amount + $interest;
$total_due += $due_amount;
$principal_diminishing -= ( $current_loan->principal/ $installments);
?>
          <div class="list-group-item">
            <span class="font110p bold pull-right"><?php echo number_format($due_amount,2); ?></span>
              <span><?php echo $i; ?></span> 
            </div>
        <?php } ?>
        </div>

        <div class="panel-footer visible-xs">
        <span class="pull-right bold font120p"><?php echo number_format($total_due,2); ?></span>
            <span>TOTAL</span> 
        </div>

      </div>


<?php } else { ?>

<center>
<a href="<?php echo site_url(str_replace('schedule','interest',uri_string())); ?>" class="btn btn-success">Add Interest</a>
</center>
<br><br>
<?php } ?>

  </div>
</div>  
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>