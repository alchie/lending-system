<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Contributed Capitals</h3>
      </div>
      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>Member Name</th>
              <th>Receipt Number</th>
              <th>Date Contributed</th>
              <th>Amount</th>
              <th width="5%">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($capitals as $capital) { ?>
            <tr>
               <td><?php echo $capital->lastname; ?>, <?php echo $capital->firstname; ?> <?php echo ($capital->middlename!='') ? substr($capital->middlename,0,1)."." : ''; ?></td>
              <td><?php echo $capital->receipt_number; ?></td>
              <td><?php echo date('F d, Y', strtotime($capital->date_contributed)); ?></td>
              <td><?php echo number_format($capital->amount,2); ?></td>
              <td><a href="<?php echo site_url( "members/capital/" . $capital->member_id . "/update/" . $capital->id ); ?>" class="btn btn-warning btn-xs">View Capital</a></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

      </div>

        <div class="list-group visible-xs">
        <?php foreach($capitals as $capital) { ?>
          <a href="<?php echo site_url( "members/capital/" . $capital->member_id . "/update/" . $capital->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format($capital->amount,2); ?></span>
              <div class="font70p"><?php echo $capital->lastname; ?>, <?php echo $capital->firstname; ?></div>
          </a>
        <?php } ?>

         <?php echo ($pagination!='') ? '<div class="list-group-item text-center">' . $pagination . '</div>' : ''; ?>

        </div>

    </div>
  </div>
</div>
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>