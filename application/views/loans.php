<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Approved Loans</h3>
      </div>
      <div class="panel-body hidden-xs">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>Lender</th>
              <th>Principal</th>
              <th>Interest</th>
              <th>Interest Type</th>
              <th>Paid</th>
              <th>Balance</th>
              <th width="4%">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($loans as $loan) { 
            
if( $loan->type == 'diminishing' ) {
  $total_interest = $loan->total_interest2;
} else {
  $total_interest = $loan->total_interest;
}

            ?>
            <tr>
               <td><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo ($loan->middlename!='') ? substr($loan->middlename,0,1)."." : ''; ?></td>
              <td><?php echo number_format($loan->principal,2); ?></td>
              <td><?php echo number_format($total_interest,2); ?></td>
              <td><?php echo ucwords($loan->type); ?></td>
              <td><?php echo number_format($loan->payments,2); ?></td>
              <td><?php echo number_format((($loan->principal+$total_interest)-$loan->payments),2); ?></td>
              <td><a href="<?php echo site_url( "members/loan/" . $loan->member_id . "/schedule/" . $loan->id ); ?>" class="btn btn-warning btn-xs">View Loan</a></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
        
<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

      </div>

      <div class="list-group visible-xs">
        <?php foreach($loans as $loan) { 

if( $loan->type == 'diminishing' ) {
  $total_interest = $loan->total_interest2;
} else {
  $total_interest = $loan->total_interest;
}
          ?>
          <a href="<?php echo site_url( "members/loan/" . $loan->member_id . "/schedule/" . $loan->id ); ?>" class="list-group-item text-center">
              <span class="font110p bold"><?php echo number_format((($loan->principal+$total_interest)-$loan->payments),2); ?></span>
              <div class="font70p"><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?></div>
          </a>
        <?php } ?>

        <?php echo ($pagination!='') ? '<div class="list-group-item text-center">' . $pagination . '</div>' : ''; ?>
        
        </div>

    </div>
  </div>
</div>
    </div> <!-- /container -->
<?php $this->load->view('footer'); ?>