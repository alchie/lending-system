<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                if( ! $this->session->loggedIn || ! isset($this->session->loggedIn) ) {
                	$this->session->sess_destroy();
                	redirect('account/login');
                }

                $this->template_data->set('session_auth', $this->session->session_auth);
        }

        public function _isAuth($module, $action='view', $uri=false) {
        	
                $auth = false;

                if( isset( $this->session->session_auth ) ) {
                	if( isset($this->session->session_auth->$module) ) {
                		if( isset($this->session->session_auth->$module->$action) ) {
                			$auth = (bool) $this->session->session_auth->$module->$action;
                		}
                	} 
                }

        	if( $auth ) {
                        
                        return $auth;

                } else {
                        
                        if( $uri == '') {

                                $uri = 'welcome';

                        }
        		redirect( $uri, 'refresh' );
        	}

        	

        }

        public function _shareHoldersOnly($uri=false) {
                if( $this->session->member_type != 'shareholder' ) {
                        if( $uri ) {
                                redirect($uri);
                        } else {
                                redirect('welcome');
                        }
                }
        }
}