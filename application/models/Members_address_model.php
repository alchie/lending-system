<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Members_address_model Class
 *
 * Manipulates `members_address` table on database

CREATE TABLE `members_address` (
  `member_id` int(20) NOT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `building` varchar(100) DEFAULT NULL,
  `lot_block` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `subdivision` varchar(100) DEFAULT NULL,
  `barangay` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  KEY `id` (`member_id`),
  CONSTRAINT `members_address_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

 * @package			Model
 * @project			Lending System
 * @project_link	http://www.trokis.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Members_address_model extends CI_Model {

	protected $member_id;
	protected $unit;
	protected $building;
	protected $lot_block;
	protected $street;
	protected $subdivision;
	protected $barangay;
	protected $city;
	protected $province;
	protected $zip;
	protected $_short_name = 'members_address';
	protected $_dataFields = array();
	protected $_select = array();
	protected $_join = array();
	protected $_where = array();
	protected $_where_or = array();
	protected $_where_in = array();
	protected $_where_in_or = array();
	protected $_where_not_in = array();
	protected $_where_not_in_or = array();
	protected $_like = array();
	protected $_like_or = array();
	protected $_like_not = array();
	protected $_like_not_or = array();
	protected $_having = array();
	protected $_having_or = array();
	protected $_group_by = array();
	protected $_filter = array();
	protected $_order = array();
	protected $_exclude = array();
	protected $_required = array();
	protected $_countField = '';
	protected $_start = 0;
	protected $_limit = 10;
	protected $_results = FALSE;
	protected $_distinct = FALSE;
	protected $_cache_on = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name='members_address') {
		parent::__construct();
		$this->_short_name = $short_name;
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: member_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `member_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMemberId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->member_id = ( ($value == '') && ($this->member_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.member_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'member_id';
		}
		return $this;
	}

	/** 
	* Get the value of `member_id` variable
	* @access public
	* @return String;
	*/

	public function getMemberId() {
		return $this->member_id;
	}

	/**
	* Get row by `member_id`
	* @param member_id
	* @return QueryResult
	**/

	public function getByMemberId() {
		if($this->member_id != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.member_id' => $this->member_id), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `member_id`
	**/

	public function updateByMemberId() {
		if($this->member_id != '') {
			$this->setExclude('member_id');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.member_id' => $this->member_id ) );
			}
		}
	}


	/**
	* Delete row by `member_id`
	**/

	public function deleteByMemberId() {
		if($this->member_id != '') {
			return $this->db->delete('members_address', array('members_address.member_id' => $this->member_id ) );
		}
	}

	/**
	* Increment row by `member_id`
	**/

	public function incrementByMemberId() {
		if($this->member_id != '' && $this->_countField != '') {
			$this->db->where('member_id', $this->member_id);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `member_id`
	**/

	public function decrementByMemberId() {
		if($this->member_id != '' && $this->_countField != '') {
			$this->db->where('member_id', $this->member_id);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: member_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: unit
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `unit` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUnit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->unit = ( ($value == '') && ($this->unit != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.unit';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'unit';
		}
		return $this;
	}

	/** 
	* Get the value of `unit` variable
	* @access public
	* @return String;
	*/

	public function getUnit() {
		return $this->unit;
	}

	/**
	* Get row by `unit`
	* @param unit
	* @return QueryResult
	**/

	public function getByUnit() {
		if($this->unit != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.unit' => $this->unit), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `unit`
	**/

	public function updateByUnit() {
		if($this->unit != '') {
			$this->setExclude('unit');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.unit' => $this->unit ) );
			}
		}
	}


	/**
	* Delete row by `unit`
	**/

	public function deleteByUnit() {
		if($this->unit != '') {
			return $this->db->delete('members_address', array('members_address.unit' => $this->unit ) );
		}
	}

	/**
	* Increment row by `unit`
	**/

	public function incrementByUnit() {
		if($this->unit != '' && $this->_countField != '') {
			$this->db->where('unit', $this->unit);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `unit`
	**/

	public function decrementByUnit() {
		if($this->unit != '' && $this->_countField != '') {
			$this->db->where('unit', $this->unit);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: unit
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: building
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `building` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setBuilding($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->building = ( ($value == '') && ($this->building != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.building';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'building';
		}
		return $this;
	}

	/** 
	* Get the value of `building` variable
	* @access public
	* @return String;
	*/

	public function getBuilding() {
		return $this->building;
	}

	/**
	* Get row by `building`
	* @param building
	* @return QueryResult
	**/

	public function getByBuilding() {
		if($this->building != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.building' => $this->building), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `building`
	**/

	public function updateByBuilding() {
		if($this->building != '') {
			$this->setExclude('building');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.building' => $this->building ) );
			}
		}
	}


	/**
	* Delete row by `building`
	**/

	public function deleteByBuilding() {
		if($this->building != '') {
			return $this->db->delete('members_address', array('members_address.building' => $this->building ) );
		}
	}

	/**
	* Increment row by `building`
	**/

	public function incrementByBuilding() {
		if($this->building != '' && $this->_countField != '') {
			$this->db->where('building', $this->building);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `building`
	**/

	public function decrementByBuilding() {
		if($this->building != '' && $this->_countField != '') {
			$this->db->where('building', $this->building);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: building
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lot_block
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lot_block` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLotBlock($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lot_block = ( ($value == '') && ($this->lot_block != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.lot_block';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'lot_block';
		}
		return $this;
	}

	/** 
	* Get the value of `lot_block` variable
	* @access public
	* @return String;
	*/

	public function getLotBlock() {
		return $this->lot_block;
	}

	/**
	* Get row by `lot_block`
	* @param lot_block
	* @return QueryResult
	**/

	public function getByLotBlock() {
		if($this->lot_block != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.lot_block' => $this->lot_block), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lot_block`
	**/

	public function updateByLotBlock() {
		if($this->lot_block != '') {
			$this->setExclude('lot_block');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.lot_block' => $this->lot_block ) );
			}
		}
	}


	/**
	* Delete row by `lot_block`
	**/

	public function deleteByLotBlock() {
		if($this->lot_block != '') {
			return $this->db->delete('members_address', array('members_address.lot_block' => $this->lot_block ) );
		}
	}

	/**
	* Increment row by `lot_block`
	**/

	public function incrementByLotBlock() {
		if($this->lot_block != '' && $this->_countField != '') {
			$this->db->where('lot_block', $this->lot_block);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `lot_block`
	**/

	public function decrementByLotBlock() {
		if($this->lot_block != '' && $this->_countField != '') {
			$this->db->where('lot_block', $this->lot_block);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lot_block
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: street
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `street` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setStreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->street = ( ($value == '') && ($this->street != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.street';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'street';
		}
		return $this;
	}

	/** 
	* Get the value of `street` variable
	* @access public
	* @return String;
	*/

	public function getStreet() {
		return $this->street;
	}

	/**
	* Get row by `street`
	* @param street
	* @return QueryResult
	**/

	public function getByStreet() {
		if($this->street != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.street' => $this->street), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `street`
	**/

	public function updateByStreet() {
		if($this->street != '') {
			$this->setExclude('street');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.street' => $this->street ) );
			}
		}
	}


	/**
	* Delete row by `street`
	**/

	public function deleteByStreet() {
		if($this->street != '') {
			return $this->db->delete('members_address', array('members_address.street' => $this->street ) );
		}
	}

	/**
	* Increment row by `street`
	**/

	public function incrementByStreet() {
		if($this->street != '' && $this->_countField != '') {
			$this->db->where('street', $this->street);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `street`
	**/

	public function decrementByStreet() {
		if($this->street != '' && $this->_countField != '') {
			$this->db->where('street', $this->street);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: street
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: subdivision
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `subdivision` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSubdivision($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->subdivision = ( ($value == '') && ($this->subdivision != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.subdivision';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'subdivision';
		}
		return $this;
	}

	/** 
	* Get the value of `subdivision` variable
	* @access public
	* @return String;
	*/

	public function getSubdivision() {
		return $this->subdivision;
	}

	/**
	* Get row by `subdivision`
	* @param subdivision
	* @return QueryResult
	**/

	public function getBySubdivision() {
		if($this->subdivision != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.subdivision' => $this->subdivision), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `subdivision`
	**/

	public function updateBySubdivision() {
		if($this->subdivision != '') {
			$this->setExclude('subdivision');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.subdivision' => $this->subdivision ) );
			}
		}
	}


	/**
	* Delete row by `subdivision`
	**/

	public function deleteBySubdivision() {
		if($this->subdivision != '') {
			return $this->db->delete('members_address', array('members_address.subdivision' => $this->subdivision ) );
		}
	}

	/**
	* Increment row by `subdivision`
	**/

	public function incrementBySubdivision() {
		if($this->subdivision != '' && $this->_countField != '') {
			$this->db->where('subdivision', $this->subdivision);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `subdivision`
	**/

	public function decrementBySubdivision() {
		if($this->subdivision != '' && $this->_countField != '') {
			$this->db->where('subdivision', $this->subdivision);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: subdivision
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: barangay
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `barangay` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setBarangay($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->barangay = ( ($value == '') && ($this->barangay != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.barangay';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'barangay';
		}
		return $this;
	}

	/** 
	* Get the value of `barangay` variable
	* @access public
	* @return String;
	*/

	public function getBarangay() {
		return $this->barangay;
	}

	/**
	* Get row by `barangay`
	* @param barangay
	* @return QueryResult
	**/

	public function getByBarangay() {
		if($this->barangay != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.barangay' => $this->barangay), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `barangay`
	**/

	public function updateByBarangay() {
		if($this->barangay != '') {
			$this->setExclude('barangay');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.barangay' => $this->barangay ) );
			}
		}
	}


	/**
	* Delete row by `barangay`
	**/

	public function deleteByBarangay() {
		if($this->barangay != '') {
			return $this->db->delete('members_address', array('members_address.barangay' => $this->barangay ) );
		}
	}

	/**
	* Increment row by `barangay`
	**/

	public function incrementByBarangay() {
		if($this->barangay != '' && $this->_countField != '') {
			$this->db->where('barangay', $this->barangay);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `barangay`
	**/

	public function decrementByBarangay() {
		if($this->barangay != '' && $this->_countField != '') {
			$this->db->where('barangay', $this->barangay);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: barangay
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: city
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `city` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->city = ( ($value == '') && ($this->city != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.city';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'city';
		}
		return $this;
	}

	/** 
	* Get the value of `city` variable
	* @access public
	* @return String;
	*/

	public function getCity() {
		return $this->city;
	}

	/**
	* Get row by `city`
	* @param city
	* @return QueryResult
	**/

	public function getByCity() {
		if($this->city != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.city' => $this->city), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `city`
	**/

	public function updateByCity() {
		if($this->city != '') {
			$this->setExclude('city');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.city' => $this->city ) );
			}
		}
	}


	/**
	* Delete row by `city`
	**/

	public function deleteByCity() {
		if($this->city != '') {
			return $this->db->delete('members_address', array('members_address.city' => $this->city ) );
		}
	}

	/**
	* Increment row by `city`
	**/

	public function incrementByCity() {
		if($this->city != '' && $this->_countField != '') {
			$this->db->where('city', $this->city);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `city`
	**/

	public function decrementByCity() {
		if($this->city != '' && $this->_countField != '') {
			$this->db->where('city', $this->city);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: city
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: province
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `province` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setProvince($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->province = ( ($value == '') && ($this->province != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.province';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'province';
		}
		return $this;
	}

	/** 
	* Get the value of `province` variable
	* @access public
	* @return String;
	*/

	public function getProvince() {
		return $this->province;
	}

	/**
	* Get row by `province`
	* @param province
	* @return QueryResult
	**/

	public function getByProvince() {
		if($this->province != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.province' => $this->province), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `province`
	**/

	public function updateByProvince() {
		if($this->province != '') {
			$this->setExclude('province');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.province' => $this->province ) );
			}
		}
	}


	/**
	* Delete row by `province`
	**/

	public function deleteByProvince() {
		if($this->province != '') {
			return $this->db->delete('members_address', array('members_address.province' => $this->province ) );
		}
	}

	/**
	* Increment row by `province`
	**/

	public function incrementByProvince() {
		if($this->province != '' && $this->_countField != '') {
			$this->db->where('province', $this->province);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `province`
	**/

	public function decrementByProvince() {
		if($this->province != '' && $this->_countField != '') {
			$this->db->where('province', $this->province);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: province
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: zip
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `zip` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setZip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->zip = ( ($value == '') && ($this->zip != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.zip';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'zip';
		}
		return $this;
	}

	/** 
	* Get the value of `zip` variable
	* @access public
	* @return String;
	*/

	public function getZip() {
		return $this->zip;
	}

	/**
	* Get row by `zip`
	* @param zip
	* @return QueryResult
	**/

	public function getByZip() {
		if($this->zip != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members_address ' . $this->_short_name, array( $this->_short_name . '.zip' => $this->zip), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `zip`
	**/

	public function updateByZip() {
		if($this->zip != '') {
			$this->setExclude('zip');
			if( $this->getData() ) {
				return $this->db->update('members_address '. $this->_short_name, $this->getData(), array('members_address.zip' => $this->zip ) );
			}
		}
	}


	/**
	* Delete row by `zip`
	**/

	public function deleteByZip() {
		if($this->zip != '') {
			return $this->db->delete('members_address', array('members_address.zip' => $this->zip ) );
		}
	}

	/**
	* Increment row by `zip`
	**/

	public function incrementByZip() {
		if($this->zip != '' && $this->_countField != '') {
			$this->db->where('zip', $this->zip);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `zip`
	**/

	public function decrementByZip() {
		if($this->zip != '' && $this->_countField != '') {
			$this->db->where('zip', $this->zip);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members_address '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: zip
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupGroupBy();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('members_address ' . $this->_short_name, 1);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->_results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->_results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('members_address');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_dataFields = array($fields);
			} else {
				$this->_dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('members_address '. $this->_short_name, $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('members_address', $this->getData() ) === TRUE ) {
				$this->member_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('members_address', $this->getData() ) === TRUE ) {
				$this->member_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->_start);
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('members_address ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->_results = $result[0];
			
			return $this->_results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->_distinct ) {
			$this->db->distinct();
		}
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->_limit > 0 ) {
			$this->db->limit( $this->_limit,$this->_start);
		}
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('members_address ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
			
		$this->_results = $query->result();
		
		return $this->_results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='member_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->_limit > 0 ) {
				$this->db->limit( $this->_limit,$this->_start);
			}
			if( $this->_select ) {
					$this->db->select( implode(',' , $this->_select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->_order ) {
				foreach( $this->_order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('members_address ' . $this->_short_name);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->_where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = $this->_short_name . '.' . $field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->_where[$key] = $this->$field;
				$this->_where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->_dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("member_id","unit","building","lot_block","street","subdivision","barangay","city","province","zip");
		if( count( $this->_dataFields ) > 0 ) {
    		            $fields = $this->_dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->_required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->_required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->_exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->_required ) ) 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}

        // --------------------------------------------------------------------
	
	/**
	* Field Conditions Clauses 
	* @access protected
	* @return Null;
	*/

	protected function __field_conditions($underCondition,$key, $value, $priority) {
		switch( $underCondition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->_join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->_join ) {
			foreach( $this->_join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'members_address'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->_filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_filter);
			$this->_filter = array();
		} else {
			$newfilter = array();
			foreach($this->_filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->_filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where);
			$this->_where = array();
		} else {
			$newwhere = array();
			foreach($this->_where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->_where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_or);
			$this->_where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->_where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->_where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in);
			$this->_where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->_where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->_where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in_or);
			$this->_where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->_where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->_where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in);
			$this->_where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->_where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->_where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in_or);
			$this->_where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->_where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->_where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like);
			$this->_like = array();
		} else {
			$newlike = array();
			foreach($this->_like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->_like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_or);
			$this->_like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->_like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->_like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not);
			$this->_like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->_like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->_like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not_or);
			$this->_like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->_like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->_like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having);
			$this->_having = array();
		} else {
			$newhaving = array();
			foreach($this->_having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->_having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having_or);
			$this->_having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->_having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->_having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->_group_by = array_merge( $this->_group_by, $fields );
		} else {
			$this->_group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->_group_by ) {
			$this->db->group_by( $this->_group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->_start = $value;
		return $this;
	}

        // --------------------------------------------------------------------

        /**
	* Get Start  
	* @access public
	*/
	public function getStart() {
		return $this->_start;
	}

        // --------------------------------------------------------------------
        
	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->_limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

        /**
	* Get Limit  
	* @access public
	*/
	public function getLimit() {
		return $this->_limit;
	}

	// --------------------------------------------------------------------
	
	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->_order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_array ( $select ) ) {
			$this->_select = array_merge( $this->_select, $select );
		} else {
			if( is_null( $index ) ) {
				$this->_select[] = $select;
			} else {
				$this->_select[$index] = $select;
			}
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		if( is_array ( $exclude ) ) {
			$this->_exclude = array_merge( $this->_exclude, $exclude );
		} else {
			$this->_exclude[] = $exclude;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->_distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('members_address ' . $this->_short_name);
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->setupJoin();
		$this->db->from('members_address ' . $this->_short_name);
		return  $this->db->count_all_results();
	}

	// --------------------------------------------------------------------

	/**
	* SQL Functions  
	* @access public
	*/
	public function sql_function($function, $field, $alias=false, $hasConditions=true) {
                if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		if( $alias ) {
                        $this->db->select($function.'('.$field.') AS ' . $alias);
		} else {
                        $this->db->select($function.'('.$field.')');
		}
		if( $hasConditions ) {
                        $this->setupJoin();
                        $this->setupConditions();
                        $this->setupGroupBy();
                }
		return  $this->db->get('members_address ' . $this->_short_name, 1);
	}	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->_cache_on = TRUE;
	}
}

/* End of file members_address_model.php */
/* Location: ./application/models/members_address_model.php */
