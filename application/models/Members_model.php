<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Members_model Class
 *
 * Manipulates `members` table on database

CREATE TABLE `members` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `member_type` varchar(20) NOT NULL DEFAULT 'lender',
  `employee` int(1) NOT NULL DEFAULT '0',
  `active` int(20) NOT NULL DEFAULT '1',
  `gender` varchar(10) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthplace` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_mobile` varchar(255) DEFAULT NULL,
  `phone_home` varchar(255) DEFAULT NULL,
  `phone_office` varchar(255) DEFAULT NULL,
  `submitted_by` int(20) DEFAULT NULL,
  `lastmod_by` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

 * @package			Model
 * @project			Lending System
 * @project_link	http://www.trokis.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Members_model extends CI_Model {

	protected $id;
	protected $firstname;
	protected $lastname;
	protected $middlename;
	protected $member_type;
	protected $employee;
	protected $active;
	protected $gender;
	protected $marital_status;
	protected $birthdate;
	protected $birthplace;
	protected $email;
	protected $phone_mobile;
	protected $phone_home;
	protected $phone_office;
	protected $submitted_by;
	protected $lastmod_by;
	protected $approved_by;
	protected $_short_name = 'members';
	protected $_dataFields = array();
	protected $_select = array();
	protected $_join = array();
	protected $_where = array();
	protected $_where_or = array();
	protected $_where_in = array();
	protected $_where_in_or = array();
	protected $_where_not_in = array();
	protected $_where_not_in_or = array();
	protected $_like = array();
	protected $_like_or = array();
	protected $_like_not = array();
	protected $_like_not_or = array();
	protected $_having = array();
	protected $_having_or = array();
	protected $_group_by = array();
	protected $_filter = array();
	protected $_order = array();
	protected $_exclude = array();
	protected $_required = array();
	protected $_countField = '';
	protected $_start = 0;
	protected $_limit = 10;
	protected $_results = FALSE;
	protected $_distinct = FALSE;
	protected $_cache_on = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name='members') {
		parent::__construct();
		$this->_short_name = $short_name;
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.id' => $this->id), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('members', array('members.id' => $this->id ) );
		}
	}

	/**
	* Increment row by `id`
	**/

	public function incrementById() {
		if($this->id != '' && $this->_countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `id`
	**/

	public function decrementById() {
		if($this->id != '' && $this->_countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: firstname
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `firstname` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->firstname = ( ($value == '') && ($this->firstname != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.firstname';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'firstname';
		}
		return $this;
	}

	/** 
	* Get the value of `firstname` variable
	* @access public
	* @return String;
	*/

	public function getFirstname() {
		return $this->firstname;
	}

	/**
	* Get row by `firstname`
	* @param firstname
	* @return QueryResult
	**/

	public function getByFirstname() {
		if($this->firstname != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.firstname' => $this->firstname), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `firstname`
	**/

	public function updateByFirstname() {
		if($this->firstname != '') {
			$this->setExclude('firstname');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.firstname' => $this->firstname ) );
			}
		}
	}


	/**
	* Delete row by `firstname`
	**/

	public function deleteByFirstname() {
		if($this->firstname != '') {
			return $this->db->delete('members', array('members.firstname' => $this->firstname ) );
		}
	}

	/**
	* Increment row by `firstname`
	**/

	public function incrementByFirstname() {
		if($this->firstname != '' && $this->_countField != '') {
			$this->db->where('firstname', $this->firstname);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `firstname`
	**/

	public function decrementByFirstname() {
		if($this->firstname != '' && $this->_countField != '') {
			$this->db->where('firstname', $this->firstname);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: firstname
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lastname
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lastname` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lastname = ( ($value == '') && ($this->lastname != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.lastname';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'lastname';
		}
		return $this;
	}

	/** 
	* Get the value of `lastname` variable
	* @access public
	* @return String;
	*/

	public function getLastname() {
		return $this->lastname;
	}

	/**
	* Get row by `lastname`
	* @param lastname
	* @return QueryResult
	**/

	public function getByLastname() {
		if($this->lastname != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.lastname' => $this->lastname), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lastname`
	**/

	public function updateByLastname() {
		if($this->lastname != '') {
			$this->setExclude('lastname');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.lastname' => $this->lastname ) );
			}
		}
	}


	/**
	* Delete row by `lastname`
	**/

	public function deleteByLastname() {
		if($this->lastname != '') {
			return $this->db->delete('members', array('members.lastname' => $this->lastname ) );
		}
	}

	/**
	* Increment row by `lastname`
	**/

	public function incrementByLastname() {
		if($this->lastname != '' && $this->_countField != '') {
			$this->db->where('lastname', $this->lastname);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `lastname`
	**/

	public function decrementByLastname() {
		if($this->lastname != '' && $this->_countField != '') {
			$this->db->where('lastname', $this->lastname);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lastname
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: middlename
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `middlename` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->middlename = ( ($value == '') && ($this->middlename != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.middlename';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'middlename';
		}
		return $this;
	}

	/** 
	* Get the value of `middlename` variable
	* @access public
	* @return String;
	*/

	public function getMiddlename() {
		return $this->middlename;
	}

	/**
	* Get row by `middlename`
	* @param middlename
	* @return QueryResult
	**/

	public function getByMiddlename() {
		if($this->middlename != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.middlename' => $this->middlename), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `middlename`
	**/

	public function updateByMiddlename() {
		if($this->middlename != '') {
			$this->setExclude('middlename');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.middlename' => $this->middlename ) );
			}
		}
	}


	/**
	* Delete row by `middlename`
	**/

	public function deleteByMiddlename() {
		if($this->middlename != '') {
			return $this->db->delete('members', array('members.middlename' => $this->middlename ) );
		}
	}

	/**
	* Increment row by `middlename`
	**/

	public function incrementByMiddlename() {
		if($this->middlename != '' && $this->_countField != '') {
			$this->db->where('middlename', $this->middlename);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `middlename`
	**/

	public function decrementByMiddlename() {
		if($this->middlename != '' && $this->_countField != '') {
			$this->db->where('middlename', $this->middlename);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: middlename
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: member_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `member_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMemberType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->member_type = ( ($value == '') && ($this->member_type != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.member_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'member_type';
		}
		return $this;
	}

	/** 
	* Get the value of `member_type` variable
	* @access public
	* @return String;
	*/

	public function getMemberType() {
		return $this->member_type;
	}

	/**
	* Get row by `member_type`
	* @param member_type
	* @return QueryResult
	**/

	public function getByMemberType() {
		if($this->member_type != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.member_type' => $this->member_type), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `member_type`
	**/

	public function updateByMemberType() {
		if($this->member_type != '') {
			$this->setExclude('member_type');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.member_type' => $this->member_type ) );
			}
		}
	}


	/**
	* Delete row by `member_type`
	**/

	public function deleteByMemberType() {
		if($this->member_type != '') {
			return $this->db->delete('members', array('members.member_type' => $this->member_type ) );
		}
	}

	/**
	* Increment row by `member_type`
	**/

	public function incrementByMemberType() {
		if($this->member_type != '' && $this->_countField != '') {
			$this->db->where('member_type', $this->member_type);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `member_type`
	**/

	public function decrementByMemberType() {
		if($this->member_type != '' && $this->_countField != '') {
			$this->db->where('member_type', $this->member_type);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: member_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: employee
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `employee` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEmployee($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->employee = ( ($value == '') && ($this->employee != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.employee';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'employee';
		}
		return $this;
	}

	/** 
	* Get the value of `employee` variable
	* @access public
	* @return String;
	*/

	public function getEmployee() {
		return $this->employee;
	}

	/**
	* Get row by `employee`
	* @param employee
	* @return QueryResult
	**/

	public function getByEmployee() {
		if($this->employee != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.employee' => $this->employee), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `employee`
	**/

	public function updateByEmployee() {
		if($this->employee != '') {
			$this->setExclude('employee');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.employee' => $this->employee ) );
			}
		}
	}


	/**
	* Delete row by `employee`
	**/

	public function deleteByEmployee() {
		if($this->employee != '') {
			return $this->db->delete('members', array('members.employee' => $this->employee ) );
		}
	}

	/**
	* Increment row by `employee`
	**/

	public function incrementByEmployee() {
		if($this->employee != '' && $this->_countField != '') {
			$this->db->where('employee', $this->employee);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `employee`
	**/

	public function decrementByEmployee() {
		if($this->employee != '' && $this->_countField != '') {
			$this->db->where('employee', $this->employee);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: employee
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->active = ( ($value == '') && ($this->active != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'active';
		}
		return $this;
	}

	/** 
	* Get the value of `active` variable
	* @access public
	* @return String;
	*/

	public function getActive() {
		return $this->active;
	}

	/**
	* Get row by `active`
	* @param active
	* @return QueryResult
	**/

	public function getByActive() {
		if($this->active != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.active' => $this->active), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `active`
	**/

	public function updateByActive() {
		if($this->active != '') {
			$this->setExclude('active');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.active' => $this->active ) );
			}
		}
	}


	/**
	* Delete row by `active`
	**/

	public function deleteByActive() {
		if($this->active != '') {
			return $this->db->delete('members', array('members.active' => $this->active ) );
		}
	}

	/**
	* Increment row by `active`
	**/

	public function incrementByActive() {
		if($this->active != '' && $this->_countField != '') {
			$this->db->where('active', $this->active);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `active`
	**/

	public function decrementByActive() {
		if($this->active != '' && $this->_countField != '') {
			$this->db->where('active', $this->active);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: active
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: gender
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `gender` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setGender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->gender = ( ($value == '') && ($this->gender != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.gender';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'gender';
		}
		return $this;
	}

	/** 
	* Get the value of `gender` variable
	* @access public
	* @return String;
	*/

	public function getGender() {
		return $this->gender;
	}

	/**
	* Get row by `gender`
	* @param gender
	* @return QueryResult
	**/

	public function getByGender() {
		if($this->gender != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.gender' => $this->gender), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `gender`
	**/

	public function updateByGender() {
		if($this->gender != '') {
			$this->setExclude('gender');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.gender' => $this->gender ) );
			}
		}
	}


	/**
	* Delete row by `gender`
	**/

	public function deleteByGender() {
		if($this->gender != '') {
			return $this->db->delete('members', array('members.gender' => $this->gender ) );
		}
	}

	/**
	* Increment row by `gender`
	**/

	public function incrementByGender() {
		if($this->gender != '' && $this->_countField != '') {
			$this->db->where('gender', $this->gender);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `gender`
	**/

	public function decrementByGender() {
		if($this->gender != '' && $this->_countField != '') {
			$this->db->where('gender', $this->gender);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: gender
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: marital_status
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `marital_status` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMaritalStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->marital_status = ( ($value == '') && ($this->marital_status != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.marital_status';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'marital_status';
		}
		return $this;
	}

	/** 
	* Get the value of `marital_status` variable
	* @access public
	* @return String;
	*/

	public function getMaritalStatus() {
		return $this->marital_status;
	}

	/**
	* Get row by `marital_status`
	* @param marital_status
	* @return QueryResult
	**/

	public function getByMaritalStatus() {
		if($this->marital_status != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.marital_status' => $this->marital_status), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `marital_status`
	**/

	public function updateByMaritalStatus() {
		if($this->marital_status != '') {
			$this->setExclude('marital_status');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.marital_status' => $this->marital_status ) );
			}
		}
	}


	/**
	* Delete row by `marital_status`
	**/

	public function deleteByMaritalStatus() {
		if($this->marital_status != '') {
			return $this->db->delete('members', array('members.marital_status' => $this->marital_status ) );
		}
	}

	/**
	* Increment row by `marital_status`
	**/

	public function incrementByMaritalStatus() {
		if($this->marital_status != '' && $this->_countField != '') {
			$this->db->where('marital_status', $this->marital_status);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `marital_status`
	**/

	public function decrementByMaritalStatus() {
		if($this->marital_status != '' && $this->_countField != '') {
			$this->db->where('marital_status', $this->marital_status);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: marital_status
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: birthdate
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `birthdate` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setBirthdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->birthdate = ( ($value == '') && ($this->birthdate != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.birthdate';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'birthdate';
		}
		return $this;
	}

	/** 
	* Get the value of `birthdate` variable
	* @access public
	* @return String;
	*/

	public function getBirthdate() {
		return $this->birthdate;
	}

	/**
	* Get row by `birthdate`
	* @param birthdate
	* @return QueryResult
	**/

	public function getByBirthdate() {
		if($this->birthdate != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.birthdate' => $this->birthdate), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `birthdate`
	**/

	public function updateByBirthdate() {
		if($this->birthdate != '') {
			$this->setExclude('birthdate');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.birthdate' => $this->birthdate ) );
			}
		}
	}


	/**
	* Delete row by `birthdate`
	**/

	public function deleteByBirthdate() {
		if($this->birthdate != '') {
			return $this->db->delete('members', array('members.birthdate' => $this->birthdate ) );
		}
	}

	/**
	* Increment row by `birthdate`
	**/

	public function incrementByBirthdate() {
		if($this->birthdate != '' && $this->_countField != '') {
			$this->db->where('birthdate', $this->birthdate);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `birthdate`
	**/

	public function decrementByBirthdate() {
		if($this->birthdate != '' && $this->_countField != '') {
			$this->db->where('birthdate', $this->birthdate);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: birthdate
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: birthplace
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `birthplace` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setBirthplace($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->birthplace = ( ($value == '') && ($this->birthplace != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.birthplace';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'birthplace';
		}
		return $this;
	}

	/** 
	* Get the value of `birthplace` variable
	* @access public
	* @return String;
	*/

	public function getBirthplace() {
		return $this->birthplace;
	}

	/**
	* Get row by `birthplace`
	* @param birthplace
	* @return QueryResult
	**/

	public function getByBirthplace() {
		if($this->birthplace != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.birthplace' => $this->birthplace), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `birthplace`
	**/

	public function updateByBirthplace() {
		if($this->birthplace != '') {
			$this->setExclude('birthplace');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.birthplace' => $this->birthplace ) );
			}
		}
	}


	/**
	* Delete row by `birthplace`
	**/

	public function deleteByBirthplace() {
		if($this->birthplace != '') {
			return $this->db->delete('members', array('members.birthplace' => $this->birthplace ) );
		}
	}

	/**
	* Increment row by `birthplace`
	**/

	public function incrementByBirthplace() {
		if($this->birthplace != '' && $this->_countField != '') {
			$this->db->where('birthplace', $this->birthplace);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `birthplace`
	**/

	public function decrementByBirthplace() {
		if($this->birthplace != '' && $this->_countField != '') {
			$this->db->where('birthplace', $this->birthplace);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: birthplace
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: email
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->email = ( ($value == '') && ($this->email != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.email';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'email';
		}
		return $this;
	}

	/** 
	* Get the value of `email` variable
	* @access public
	* @return String;
	*/

	public function getEmail() {
		return $this->email;
	}

	/**
	* Get row by `email`
	* @param email
	* @return QueryResult
	**/

	public function getByEmail() {
		if($this->email != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.email' => $this->email), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `email`
	**/

	public function updateByEmail() {
		if($this->email != '') {
			$this->setExclude('email');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.email' => $this->email ) );
			}
		}
	}


	/**
	* Delete row by `email`
	**/

	public function deleteByEmail() {
		if($this->email != '') {
			return $this->db->delete('members', array('members.email' => $this->email ) );
		}
	}

	/**
	* Increment row by `email`
	**/

	public function incrementByEmail() {
		if($this->email != '' && $this->_countField != '') {
			$this->db->where('email', $this->email);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `email`
	**/

	public function decrementByEmail() {
		if($this->email != '' && $this->_countField != '') {
			$this->db->where('email', $this->email);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: email
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: phone_mobile
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `phone_mobile` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPhoneMobile($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->phone_mobile = ( ($value == '') && ($this->phone_mobile != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.phone_mobile';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'phone_mobile';
		}
		return $this;
	}

	/** 
	* Get the value of `phone_mobile` variable
	* @access public
	* @return String;
	*/

	public function getPhoneMobile() {
		return $this->phone_mobile;
	}

	/**
	* Get row by `phone_mobile`
	* @param phone_mobile
	* @return QueryResult
	**/

	public function getByPhoneMobile() {
		if($this->phone_mobile != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.phone_mobile' => $this->phone_mobile), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `phone_mobile`
	**/

	public function updateByPhoneMobile() {
		if($this->phone_mobile != '') {
			$this->setExclude('phone_mobile');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.phone_mobile' => $this->phone_mobile ) );
			}
		}
	}


	/**
	* Delete row by `phone_mobile`
	**/

	public function deleteByPhoneMobile() {
		if($this->phone_mobile != '') {
			return $this->db->delete('members', array('members.phone_mobile' => $this->phone_mobile ) );
		}
	}

	/**
	* Increment row by `phone_mobile`
	**/

	public function incrementByPhoneMobile() {
		if($this->phone_mobile != '' && $this->_countField != '') {
			$this->db->where('phone_mobile', $this->phone_mobile);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `phone_mobile`
	**/

	public function decrementByPhoneMobile() {
		if($this->phone_mobile != '' && $this->_countField != '') {
			$this->db->where('phone_mobile', $this->phone_mobile);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: phone_mobile
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: phone_home
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `phone_home` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPhoneHome($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->phone_home = ( ($value == '') && ($this->phone_home != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.phone_home';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'phone_home';
		}
		return $this;
	}

	/** 
	* Get the value of `phone_home` variable
	* @access public
	* @return String;
	*/

	public function getPhoneHome() {
		return $this->phone_home;
	}

	/**
	* Get row by `phone_home`
	* @param phone_home
	* @return QueryResult
	**/

	public function getByPhoneHome() {
		if($this->phone_home != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.phone_home' => $this->phone_home), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `phone_home`
	**/

	public function updateByPhoneHome() {
		if($this->phone_home != '') {
			$this->setExclude('phone_home');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.phone_home' => $this->phone_home ) );
			}
		}
	}


	/**
	* Delete row by `phone_home`
	**/

	public function deleteByPhoneHome() {
		if($this->phone_home != '') {
			return $this->db->delete('members', array('members.phone_home' => $this->phone_home ) );
		}
	}

	/**
	* Increment row by `phone_home`
	**/

	public function incrementByPhoneHome() {
		if($this->phone_home != '' && $this->_countField != '') {
			$this->db->where('phone_home', $this->phone_home);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `phone_home`
	**/

	public function decrementByPhoneHome() {
		if($this->phone_home != '' && $this->_countField != '') {
			$this->db->where('phone_home', $this->phone_home);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: phone_home
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: phone_office
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `phone_office` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPhoneOffice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->phone_office = ( ($value == '') && ($this->phone_office != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.phone_office';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'phone_office';
		}
		return $this;
	}

	/** 
	* Get the value of `phone_office` variable
	* @access public
	* @return String;
	*/

	public function getPhoneOffice() {
		return $this->phone_office;
	}

	/**
	* Get row by `phone_office`
	* @param phone_office
	* @return QueryResult
	**/

	public function getByPhoneOffice() {
		if($this->phone_office != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.phone_office' => $this->phone_office), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `phone_office`
	**/

	public function updateByPhoneOffice() {
		if($this->phone_office != '') {
			$this->setExclude('phone_office');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.phone_office' => $this->phone_office ) );
			}
		}
	}


	/**
	* Delete row by `phone_office`
	**/

	public function deleteByPhoneOffice() {
		if($this->phone_office != '') {
			return $this->db->delete('members', array('members.phone_office' => $this->phone_office ) );
		}
	}

	/**
	* Increment row by `phone_office`
	**/

	public function incrementByPhoneOffice() {
		if($this->phone_office != '' && $this->_countField != '') {
			$this->db->where('phone_office', $this->phone_office);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `phone_office`
	**/

	public function decrementByPhoneOffice() {
		if($this->phone_office != '' && $this->_countField != '') {
			$this->db->where('phone_office', $this->phone_office);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: phone_office
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: submitted_by
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `submitted_by` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSubmittedBy($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->submitted_by = ( ($value == '') && ($this->submitted_by != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.submitted_by';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'submitted_by';
		}
		return $this;
	}

	/** 
	* Get the value of `submitted_by` variable
	* @access public
	* @return String;
	*/

	public function getSubmittedBy() {
		return $this->submitted_by;
	}

	/**
	* Get row by `submitted_by`
	* @param submitted_by
	* @return QueryResult
	**/

	public function getBySubmittedBy() {
		if($this->submitted_by != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.submitted_by' => $this->submitted_by), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `submitted_by`
	**/

	public function updateBySubmittedBy() {
		if($this->submitted_by != '') {
			$this->setExclude('submitted_by');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.submitted_by' => $this->submitted_by ) );
			}
		}
	}


	/**
	* Delete row by `submitted_by`
	**/

	public function deleteBySubmittedBy() {
		if($this->submitted_by != '') {
			return $this->db->delete('members', array('members.submitted_by' => $this->submitted_by ) );
		}
	}

	/**
	* Increment row by `submitted_by`
	**/

	public function incrementBySubmittedBy() {
		if($this->submitted_by != '' && $this->_countField != '') {
			$this->db->where('submitted_by', $this->submitted_by);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `submitted_by`
	**/

	public function decrementBySubmittedBy() {
		if($this->submitted_by != '' && $this->_countField != '') {
			$this->db->where('submitted_by', $this->submitted_by);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: submitted_by
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lastmod_by
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lastmod_by` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastmodBy($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lastmod_by = ( ($value == '') && ($this->lastmod_by != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.lastmod_by';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'lastmod_by';
		}
		return $this;
	}

	/** 
	* Get the value of `lastmod_by` variable
	* @access public
	* @return String;
	*/

	public function getLastmodBy() {
		return $this->lastmod_by;
	}

	/**
	* Get row by `lastmod_by`
	* @param lastmod_by
	* @return QueryResult
	**/

	public function getByLastmodBy() {
		if($this->lastmod_by != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.lastmod_by' => $this->lastmod_by), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lastmod_by`
	**/

	public function updateByLastmodBy() {
		if($this->lastmod_by != '') {
			$this->setExclude('lastmod_by');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.lastmod_by' => $this->lastmod_by ) );
			}
		}
	}


	/**
	* Delete row by `lastmod_by`
	**/

	public function deleteByLastmodBy() {
		if($this->lastmod_by != '') {
			return $this->db->delete('members', array('members.lastmod_by' => $this->lastmod_by ) );
		}
	}

	/**
	* Increment row by `lastmod_by`
	**/

	public function incrementByLastmodBy() {
		if($this->lastmod_by != '' && $this->_countField != '') {
			$this->db->where('lastmod_by', $this->lastmod_by);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `lastmod_by`
	**/

	public function decrementByLastmodBy() {
		if($this->lastmod_by != '' && $this->_countField != '') {
			$this->db->where('lastmod_by', $this->lastmod_by);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lastmod_by
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: approved_by
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `approved_by` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setApprovedBy($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->approved_by = ( ($value == '') && ($this->approved_by != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.approved_by';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'approved_by';
		}
		return $this;
	}

	/** 
	* Get the value of `approved_by` variable
	* @access public
	* @return String;
	*/

	public function getApprovedBy() {
		return $this->approved_by;
	}

	/**
	* Get row by `approved_by`
	* @param approved_by
	* @return QueryResult
	**/

	public function getByApprovedBy() {
		if($this->approved_by != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('members ' . $this->_short_name, array( $this->_short_name . '.approved_by' => $this->approved_by), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `approved_by`
	**/

	public function updateByApprovedBy() {
		if($this->approved_by != '') {
			$this->setExclude('approved_by');
			if( $this->getData() ) {
				return $this->db->update('members '. $this->_short_name, $this->getData(), array('members.approved_by' => $this->approved_by ) );
			}
		}
	}


	/**
	* Delete row by `approved_by`
	**/

	public function deleteByApprovedBy() {
		if($this->approved_by != '') {
			return $this->db->delete('members', array('members.approved_by' => $this->approved_by ) );
		}
	}

	/**
	* Increment row by `approved_by`
	**/

	public function incrementByApprovedBy() {
		if($this->approved_by != '' && $this->_countField != '') {
			$this->db->where('approved_by', $this->approved_by);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `approved_by`
	**/

	public function decrementByApprovedBy() {
		if($this->approved_by != '' && $this->_countField != '') {
			$this->db->where('approved_by', $this->approved_by);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('members '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: approved_by
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupGroupBy();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('members ' . $this->_short_name, 1);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->_results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->_results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('members');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_dataFields = array($fields);
			} else {
				$this->_dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('members '. $this->_short_name, $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('members', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('members', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->_start);
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('members ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->_results = $result[0];
			
			return $this->_results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->_distinct ) {
			$this->db->distinct();
		}
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->_limit > 0 ) {
			$this->db->limit( $this->_limit,$this->_start);
		}
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('members ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
			
		$this->_results = $query->result();
		
		return $this->_results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->_limit > 0 ) {
				$this->db->limit( $this->_limit,$this->_start);
			}
			if( $this->_select ) {
					$this->db->select( implode(',' , $this->_select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->_order ) {
				foreach( $this->_order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('members ' . $this->_short_name);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->_where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = $this->_short_name . '.' . $field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->_where[$key] = $this->$field;
				$this->_where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->_dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("id","firstname","lastname","middlename","member_type","employee","active","gender","marital_status","birthdate","birthplace","email","phone_mobile","phone_home","phone_office","submitted_by","lastmod_by","approved_by");
		if( count( $this->_dataFields ) > 0 ) {
    		            $fields = $this->_dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->_required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->_required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->_exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->_required ) ) 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}

        // --------------------------------------------------------------------
	
	/**
	* Field Conditions Clauses 
	* @access protected
	* @return Null;
	*/

	protected function __field_conditions($underCondition,$key, $value, $priority) {
		switch( $underCondition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->_join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->_join ) {
			foreach( $this->_join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'members'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->_filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_filter);
			$this->_filter = array();
		} else {
			$newfilter = array();
			foreach($this->_filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->_filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where);
			$this->_where = array();
		} else {
			$newwhere = array();
			foreach($this->_where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->_where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_or);
			$this->_where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->_where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->_where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in);
			$this->_where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->_where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->_where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in_or);
			$this->_where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->_where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->_where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in);
			$this->_where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->_where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->_where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in_or);
			$this->_where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->_where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->_where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like);
			$this->_like = array();
		} else {
			$newlike = array();
			foreach($this->_like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->_like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_or);
			$this->_like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->_like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->_like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not);
			$this->_like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->_like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->_like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not_or);
			$this->_like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->_like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->_like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having);
			$this->_having = array();
		} else {
			$newhaving = array();
			foreach($this->_having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->_having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having_or);
			$this->_having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->_having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->_having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->_group_by = array_merge( $this->_group_by, $fields );
		} else {
			$this->_group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->_group_by ) {
			$this->db->group_by( $this->_group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->_start = $value;
		return $this;
	}

        // --------------------------------------------------------------------

        /**
	* Get Start  
	* @access public
	*/
	public function getStart() {
		return $this->_start;
	}

        // --------------------------------------------------------------------
        
	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->_limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

        /**
	* Get Limit  
	* @access public
	*/
	public function getLimit() {
		return $this->_limit;
	}

	// --------------------------------------------------------------------
	
	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->_order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_array ( $select ) ) {
			$this->_select = array_merge( $this->_select, $select );
		} else {
			if( is_null( $index ) ) {
				$this->_select[] = $select;
			} else {
				$this->_select[$index] = $select;
			}
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		if( is_array ( $exclude ) ) {
			$this->_exclude = array_merge( $this->_exclude, $exclude );
		} else {
			$this->_exclude[] = $exclude;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->_distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('members ' . $this->_short_name);
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->setupJoin();
		$this->db->from('members ' . $this->_short_name);
		return  $this->db->count_all_results();
	}

	// --------------------------------------------------------------------

	/**
	* SQL Functions  
	* @access public
	*/
	public function sql_function($function, $field, $alias=false, $hasConditions=true) {
                if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		if( $alias ) {
                        $this->db->select($function.'('.$field.') AS ' . $alias);
		} else {
                        $this->db->select($function.'('.$field.')');
		}
		if( $hasConditions ) {
                        $this->setupJoin();
                        $this->setupConditions();
                        $this->setupGroupBy();
                }
		return  $this->db->get('members ' . $this->_short_name, 1);
	}	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->_cache_on = TRUE;
	}
}

/* End of file members_model.php */
/* Location: ./application/models/members_model.php */
